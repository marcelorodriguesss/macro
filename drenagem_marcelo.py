# coding: utf-8

import arcpy
import arcpy.mapping

# Verificar as licencias
arcpy.CheckOutExtension("spatial")
arcpy.CheckOutExtension("3D")

# dir raiz
UFC11 = r'C:\UFC\UFC11'

# Pegar o Arquivo Raster do usuario
Raster = arcpy.GetParameterAsText(0)
if Raster == '#' or not Raster:
    Raster = r'{0}\Raster\REXT.tif'.format(UFC11) # fornecido pelo usuario

# Pegar o Poligono de Corte do usuario
Polig = arcpy.GetParameterAsText(1)
if Polig == '#' or not Polig:
    arcpy.AddMessage("")
else:
    # Iniciar Variavel Temporal
    Rtemp = r'{0}\Raster\RTTD.tif'.format(UFC11)
    # Apagar Arquivo Anterior
    arcpy.Delete_management(Rtemp, "RasterDataset")
    # Cortar o Raster com o Poligono
    arcpy.gp.ExtractByMask_sa(Raster, Polig, Rtemp)
    # Apagar Raster Anterior
    arcpy.Delete_management(Raster, "RasterDataset")
    # Renomear Arquivo Raster Cortado
    arcpy.Rename_management(Rtemp, Raster, "RasterDataset")

# Variaveis Locais
FD = r'{0}\Raster\FD.tif'.format(UFC11)
FA = r'{0}\Raster\FA.tif'.format(UFC11)
RC = r'{0}\Raster\mcal.tif'.format(UFC11)
SL = r'{0}\Raster\slink.tif'.format(UFC11)
SO = r'{0}\Raster\sorder.tif'.format(UFC11)
TRel = r'{0}\Raster\emrrel.tif'.format(UFC11)
Rios = r'{0}\Shape\Rios2Dt.shp'.format(UFC11)
Rios2D = r'{0}\Shape\Rios2D.shp'.format(UFC11)
VRios  = r'{0}\Shape\VRios.shp'.format(UFC11)
Shape  = r'{0}\Shape'.format(UFC11)
rtemp  = r'{0}\Shape\prtemp.shp'.format(UFC11)

# Apagar arquivos anteriores
arcpy.AddMessage(" + Apagando Arquivos Anteriores")
arcpy.Delete_management(TRel, "RasterDataset")
arcpy.Delete_management(FD, "RasterDataset")
arcpy.Delete_management(FA, "RasterDataset")
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(Rios2D, "ShapeFile")
arcpy.Delete_management(VRios, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")

# Encher os Buracos do DEM
arcpy.AddMessage(" + Prenchendo os Buracos do DEM")
arcpy.gp.Fill_sa(Raster, TRel, "")

# Obter o Flow Direction do DEM
arcpy.AddMessage(" + Calculo da Direcao do Fluxo do DEM")
arcpy.gp.FlowDirection_sa(TRel, FD, "NORMAL", "")

# Obter o Flow Accumulation do DEM
arcpy.AddMessage(" + Calculo do Fluxo Acumulado do DEM")
arcpy.gp.FlowAccumulation_sa(FD, FA, "", "FLOAT")

# Classificar os Rios pela quantidade de area drenada

from arcpy.sa import *

# Indicar o valor minimo do FA
acum = 4

arcpy.AddMessage(" + acum type {0}".format(type(acum)))

# Condicao do Raster
outRaster = Raster(FA) > acum

raise SystemExit

# Salvar o Raster
outRaster.save(RC)

# Unir os Pontos continuos dos Rios
arcpy.gp.StreamLink_sa(RC, FD, SL)

# Definir a Ordem dos Rios
arcpy.AddMessage(" + Clasificando Rios")
arcpy.gp.StreamOrder_sa(SL, FD, SO, "STRAHLER")

# Exportar os rios a Shapefile
arcpy.AddMessage(" + Exportando Rios para Shapefile")
arcpy.gp.StreamToFeature_sa(SO, FD, Rios, "SIMPLIFY")

# Criar Shapefile com Propiedades Ativas de Cotas
arcpy.CreateFeatureclass_management(Shape, "prtemp.shp", "POLYLINE", "", "ENABLED", "ENABLED", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119522E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")

# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\\UFC\\UFC11\\Shape\\prtemp.shp;C:\\UFC\\UFC11\\Shape\\Rios2Dt.shp", Rios2D, "")

# Exportar os Vertices a Pontos
arcpy.AddMessage(" + Exportando Vertices dos Rios para Shapefile")
arcpy.FeatureVerticesToPoints_management(Rios2D, VRios, "ALL")

# Apagar Arquivos Temporais
arcpy.AddMessage(" + Apagando Arquivos Temporarios")
arcpy.Delete_management(TRel, "RasterDataset")
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")

# Verificar as licencias
arcpy.CheckInExtension("Spatial")

# Definir os Arquivos Shape a inserir
arquivo1 = r"C:\UFC\UFC11\Dados\PAnalise.shp"
arquivo2 = r"C:\UFC\UFC11\Shape\Rios2D.shp"
arquivo3 = r"C:\UFC\UFC11\Shape\VRios.shp"

# Definir o Arquivo de ArcGIS atual.
tatual = arcpy.mapping.MapDocument("Current")

# Definir o Layer Atual
camadaatual = arcpy.mapping.ListDataFrames(tatual,"*")[0]

# Criar uma Nova Camada
camada1 = arcpy.mapping.Layer(arquivo1)
camada2 = arcpy.mapping.Layer(arquivo2)
camada3 = arcpy.mapping.Layer(arquivo3)

# Adicionar a Nova Camada no arquivo atual
arcpy.AddMessage(" + Adicionando Arquivos na Tela Atual")
arcpy.mapping.AddLayer(camadaatual, camada3, "Bottom")
arcpy.mapping.AddLayer(camadaatual, camada2, "Bottom")
arcpy.mapping.AddLayer(camadaatual, camada1, "Bottom")

# Atualizar los Arquivos
arcpy.RefreshActiveView()
arcpy.RefreshTOC()

# Zoom Shapefile PAnalise.shp
# Escolher o Documento Atual
mxd = arcpy.mapping.MapDocument('current')

# Escolher o Layer Atual
df = arcpy.mapping.ListDataFrames(mxd)[0]

# Escolher o Shape da Bacia
lyr = arcpy.mapping.ListLayers(mxd, 'PAnalise*', df)[0]

# Definir a Variavel pra fazer Zoom
ext = lyr.getExtent()

# Fazer Zoom to Layer
df.extent = ext
df.scale = 5000
arcpy.RefreshActiveView()
