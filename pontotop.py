# Importar arcpy module (Modulo do Arcgis)
import arcpy
# Importar zipfile Module (Livraria do Python)
import zipfile
# Variaveis Locais
PPonto = "C:\\UFC\\UFC11\\Dados\\Ponto"
Pontos = "C:\\UFC\\UFC11\\Saida\\pontos.shp"
pontoslab = "C:\\UFC\\UFC11\\Saida\\pontos_label.shp"
Poligono = "C:\\UFC\\UFC11\\Saida\\Pol.shp"
PolWGS = "C:\\UFC\\UFC11\\Shape\\Poligono.shp"
Ponto = "C:\\UFC\\UFC11\\Dados\\Ponto\\Ponto.shp"
# Apagar arquivos e pasta existentes
arcpy.AddMessage("Apagando Arquivos Anteriores")
arcpy.Delete_management(PPonto, "Folder")
arcpy.Delete_management(Pontos, "ShapeFile")
arcpy.Delete_management(pontoslab, "ShapeFile")
arcpy.Delete_management(Poligono, "ShapeFile")
arcpy.Delete_management(PolWGS, "ShapeFile")
arcpy.Delete_management(Ponto, "ShapeFile")
# Pega a Zona Geografica do usuario
SCOR = arcpy.GetParameterAsText(0)
if SCOR == '#' or not SCOR:
	SCOR = "PROJCS['WGS_1984_UTM_Zone_24S',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',10000000.0],PARAMETER['Central_Meridian',-39.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]" # provide a default value if unspecified
# Pega o Dado fornecido pelo usuario no campo Leste e Norte para o Ponto 1.
leste1 = arcpy.GetParameterAsText(1)
norte1 = arcpy.GetParameterAsText(2)
# Pega o Dado fornecido pelo usuario no campo Leste e Norte para o Ponto 2.
leste2 = arcpy.GetParameterAsText(3)
norte2 = arcpy.GetParameterAsText(4)
# Pega o Dado fornecido pelo usuario para o Delta X e Delta Y.
deltax = arcpy.GetParameterAsText(5)
deltay = arcpy.GetParameterAsText(6)
# Pega o Intervalo das curvas de Nivel
Intervalo = arcpy.GetParameterAsText(7)
# Calculo do Numero em X e Y
numx = int((float(leste2)-float(leste1))/float(deltax))+1
numy = int((float(norte1)-float(norte2))/float(deltay))+1
# Calcular deltax e deltay corrigido
deltaxc = (float(leste2)-float(leste1))/(float(numx)-1)
deltayc = (float(norte1)-float(norte2))/(float(numy)-1)
# Modificar os Vertices
leste1b = float(leste1) - float(deltaxc)
norte1b = float(norte1) + float(deltayc)
leste2b = float(leste2) + float(deltaxc)
norte2b = float(norte2) - float(deltayc)
# Parametros para o Processo
par1 = str(leste1b) +  " " + str(norte2b)
par2 = str(leste1b) +  " " + str(float(norte2b)+10)
par3 = str(leste2b) +  " " + str(norte1b)
par4 = str(leste1b) +  " " + str(norte2b) +  " " + str(leste2b) +  " " + str(norte1b)
# Process: Create Fishnet
arcpy.CreateFishnet_management(Pontos, par1, par2, "", "", numy, numx, par3, "LABELS", par4, "POLYLINE")
# Projetar Arquivos de Pontos
arcpy.DefineProjection_management(pontoslab, SCOR)
# Apagar Arquivo Pontos Poligonos
arcpy.Delete_management(Pontos, "ShapeFile")
# Renomear Arquivo
arcpy.Rename_management(pontoslab, Pontos, "")
# Criar Poligono para Descarregar Topografia
# Extrair Arquivo Poligono.zip na Pasta Dados.
arcpy.AddMessage("Extraindo Arquivos")
poligono = zipfile.ZipFile('C:\UFC\UFC11\Dados\Poligono.zip')
poligono.extractall('C:\UFC\UFC11\Saida')
poligono.close()
# Define a Projecao do Arquivo do Poligono.
arcpy.DefineProjection_management(Poligono, SCOR)
# Limites do Poligono
# Definir Floga
dleste = (float(leste2)-float(leste1))
dnorte = (float(norte1)-float(norte2))
if dleste >= dnorte:
	distf = dleste
else:
	distf = dnorte
lestemin = float(leste1)-distf*0.10
lestemax = float(leste2)+distf*0.10
nortemin = float(norte2)-distf*0.10
nortemax = float(norte1)+distf*0.10
# Coordenadas dos Vertices
ponto1 = arcpy.Point(lestemin, nortemax)
ponto2 = arcpy.Point(lestemax, nortemax)
ponto3 = arcpy.Point(lestemax, nortemin)
ponto4 = arcpy.Point(lestemin, nortemin)
# Criar o Poligono
# Inserir Fila Nova
rowInserter = arcpy.InsertCursor(Poligono)
# Criar Coordenadas dos Vertices
array = arcpy.Array([ponto1,ponto2,ponto3,ponto4])
poligon = arcpy.Polygon(array)
# Inserir os Vertices
novopol = rowInserter.newRow()
novopol.Shape = poligon
# Inserir o Poligono
rowInserter.insertRow(novopol)
# Projeta o Shape do Poligono pra WGS84
arcpy.Project_management(Poligono, PolWGS, "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]", "", "PROJCS['WGS_1984_UTM_Zone_24S',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',10000000.0],PARAMETER['Central_Meridian',-39.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]")
# Adicionar Coluna bandeira no Poligono
arcpy.AddField_management(PolWGS, "Tipo", "SHORT", "4", "", "", "", "NULLABLE", "NON_REQUIRED", "")
# Calcular Coluna bandeira
arcpy.CalculateField_management(PolWGS, "Tipo", "1", "VB", "")
# Variaveis Locais
Grade = "C:\\UFC\\UFC11\\Dados\\GradeTopodata.shp"
GTopo = "C:\UFC\UFC11\Shape\GTopo.shp"
Tabela = "C:\UFC\UFC11\Shape\Tabela.xls"
Raster = "C:\\UFC\\UFC11\\Raster"
UFC11 = "C:\\UFC\\UFC11"
# Apagar Pasta Raster
arcpy.AddMessage("Apagando Pastas Anteriores")
arcpy.Delete_management(Raster, "Folder")   # Pasta Raster
# Criar Pasta Raster
arcpy.AddMessage("Criando Pastas Novas")
arcpy.CreateFolder_management(UFC11, "Raster")  # Pasta Raster
# Apagar arquivos existentes
arcpy.AddMessage("Apagando Arquivos Anteriores")
arcpy.Delete_management(GTopo, "ShapeFile")
arcpy.Delete_management(Tabela, "File")
# Processo Cortar Grade
arcpy.Clip_analysis(Grade, PolWGS, GTopo, "")
# Exportar Tabela
arcpy.TableToExcel_conversion(GTopo, Tabela, "NAME", "CODE")
# Apagar Arquivos
arcpy.AddMessage("Apagando Arquivos Anteriores")
arcpy.Delete_management(GTopo, "ShapeFile")
# Importar Livrarias
import xlrd  # Biblioteca para leitura de arquivo no formato .xls
import urllib  # Biblioteca para download.
import zipfile  # Biblioteca para leitura de arquivos zipados.
import os.path  # Biblioteca para verificar a existencia de arquivos.
import os
# Verificar as licencas
arcpy.CheckOutExtension("spatial")
arcpy.CheckOutExtension("3D")
workbook = xlrd.open_workbook('C:\UFC\UFC11\Shape\Tabela.xls')  # Abre a tabela.
worksheet = workbook.sheet_by_name('Tabela')  # Busca a aba de nome Tabela.
# Leitura das linhas preenchidas na aba Tabela.
for row_num in xrange(worksheet.nrows):
    if row_num == 0:  # Para primeira linha.
        lista = worksheet.row_values(row_num)  # Lista dos valores da primeira linha.
        i = 0
        while i < len(lista):
            if lista[i] == "URL":  # Verifica a coluna de valor URL
                URL = i
            if lista[i] == "TOPOID":  # Verifica a coluna de valor TOPOID
                TOPOID = i
            i = i + 1
        continue
    row = worksheet.row_values(row_num)  # Lista dos valores da linha.
    url = row[URL]  # Coluna de valores do URL
    filename = "C:\UFC\UFC11\Topodata\%sZN.zip" % row[TOPOID]  # Coluna de valores do TOPOID
    # Download de arquivos.
    arcpy.AddMessage("Baixando Arquivos do Servidor TOPODATA")
    if os.path.exists(filename) == 1:  # Verifica a existencia do arquivo.
        print filename + " arquivo baixado."
    else:
        print "Baixando %s." % url
        urllib.urlretrieve(url, filename)  # Download do arquivo.
        print filename + " arquivo baixado."
    zf = zipfile.ZipFile(filename, 'r')  # Leitura do arquivo zipado.
    print "Extraindo %s." % filename
    zf.extractall("C:\UFC\UFC11\RASTER")  # Extracao do arquivo zipado.
#Variavel para a pasta dos raster
pasta = 'C:\UFC\UFC11\Raster'
#Vetor Vacuo para os arquivos rasters
rasters = []
#Vetor com todos os arquivos da Pasta
arquivos = os.walk(pasta)
#os.walk()Vetor pasta e arquivos
#Cria um Vetor dos arquivos da Pasta Raster
for root, dirs, files in arquivos:
    for arquivo in files:
        (nomearquivo, extensao) = os.path.splitext(arquivo)
        rasters.append(nomearquivo+extensao)
# Conta o numero do arquivos da pasta Raster
nro = len(rasters)
arcpy.AddMessage("Uniao do Mosaico de Imagens")
#Para o numero de arquivos 1
if nro == 1:
# Muda o nome do arquivo a raster.tif.
    os.rename( "C:\\UFC\\UFC11\\Raster\\" + rasters[0], "C:\\UFC\\UFC11\\Raster\\" + "raster.tif")
else:
	os.rename( "C:\\UFC\\UFC11\\Raster\\" + rasters[0], "C:\\UFC\\UFC11\\Raster\\" + "raster1.tif")
	os.rename( "C:\\UFC\\UFC11\\Raster\\" + rasters[1], "C:\\UFC\\UFC11\\Raster\\" + "raster2.tif")
# Pergunta se os arquivos sao dois
	if nro == 2:
		Raster = "C:\\UFC\\UFC11\\Raster"
		raster1 = "C:\\UFC\\UFC11\\Raster\\raster1.tif"
		raster2 = "C:\\UFC\\UFC11\\Raster\\raster2.tif"
		arcpy.MosaicToNewRaster_management("C:\\UFC\\UFC11\\Raster\\raster1.tif;C:\\UFC\\UFC11\\Raster\\raster2.tif", Raster, "raster.tif", "", "32_BIT_FLOAT", "", "1", "LAST", "FIRST")
# Process: Delete raster1
		arcpy.Delete_management(raster1, "RasterDataset")
# Process: Delete raster2
		arcpy.Delete_management(raster2, "RasterDataset")
	else:
		for nro in range(2,nro):
# Local variables:
			raster1 = "C:\\UFC\\UFC11\\Raster\\raster1.tif"
			raster2 = "C:\\UFC\\UFC11\\Raster\\raster2.tif"
			Raster = "C:\\UFC\\UFC11\\Raster"
			raster = "C:\\UFC\\UFC11\\Raster\\raster.tif"
# Process: Mosaic To New Raster
			arcpy.MosaicToNewRaster_management("C:\\UFC\\UFC11\\Raster\\raster1.tif;C:\\UFC\\UFC11\\Raster\\raster2.tif", Raster, "raster.tif", "", "32_BIT_FLOAT", "", "1", "LAST", "FIRST")
# Process: Delete raster1
			arcpy.Delete_management(raster1, "RasterDataset")
# Process: Delete raster2
			arcpy.Delete_management(raster2, "RasterDataset")
# Process: Rename raster to raster1
			arcpy.Rename_management(raster, raster1, "RasterDataset")
			os.rename( "C:\\UFC\\UFC11\\Raster\\" + rasters[nro], "C:\\UFC\\UFC11\\Raster\\" + "raster2.tif")
# Uniao Raster Final.
		arcpy.MosaicToNewRaster_management("C:\\UFC\\UFC11\\Raster\\raster1.tif;C:\\UFC\\UFC11\\Raster\\raster2.tif", Raster, "raster.tif", "", "32_BIT_FLOAT", "", "1", "LAST", "FIRST")
# Process: Delete raster1
		arcpy.Delete_management(raster1, "RasterDataset")
# Process: Delete raster2
		arcpy.Delete_management(raster2, "RasterDataset")
# Pegar os dados pelo usuario
RCortar = "C:\\UFC\\UFC11\\Raster\\raster.tif" # provide a default value if unspecified
# Variaveis Locais
REXT = "C:\UFC\UFC11\Raster\REXT.tif"
# Process: Define Projection
arcpy.AddMessage("Projetando Arquivo Raster")
arcpy.DefineProjection_management(RCortar, "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Process: Extract by Mask
arcpy.AddMessage("Cortando Arquivo Raster")
arcpy.gp.ExtractByMask_sa(RCortar, PolWGS, REXT)
# Apagar Arquivo Raster Mosaico
arcpy.Delete_management(RCortar, "RasterDataset")
# Apagar arquivos existentes
arcpy.AddMessage("Apagando Arquivos Temporarios")
arcpy.Delete_management(Tabela, "File")
# Variaveis Locais
REXTPRO = "C:\UFC\UFC11\Raster\cota.tif"
PCota = "C:\\UFC\\UFC11\\Saida\\PontosCota.txt"
# Apagar Arquivo Anteriores
arcpy.Delete_management(REXTPRO, "RasterDataset")
arcpy.Delete_management(PCota, "File")
# Projetar Raster
arcpy.ProjectRaster_management(REXT, REXTPRO, SCOR, "NEAREST", "30.7775238456663 30.7775238456662", "", "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Inserir cotas nos Pontos
arcpy.gp.ExtractMultiValuesToPoints_sa(Pontos, "C:\\UFC\\UFC11\\Raster\\cota.tif cota", "NONE")
# Exportar Pontos com Cotas em Arquivo txt
arcpy.ExportXYv_stats(Pontos, "cota", "COMMA", PCota, "NO_FIELD_NAMES")
# Criar Curvas de Nivel
# Abrir Modulo Spatial
arcpy.CheckOutExtension("spatial")
# Local variables:
REXTPr = "C:\\UFC\\UFC11\\Raster\\REXTPr.tif"
curvas = "C:\\UFC\\UFC11\\Saida\\CurvasNivel.shp"
CurvasDWG = "C:\\UFC\\UFC11\\Saida\\CurvasNivel.dwg"
# Apagar Arquivos Anteriores
arcpy.Delete_management(REXTPr, "RasterDataset")
arcpy.Delete_management(curvas, "ShapeFile")
arcpy.Delete_management(CurvasDWG, "CadDrawingDataset")
# Projetar Raster
arcpy.ProjectRaster_management(REXT, REXTPr, SCOR, "NEAREST", "30.7734005649248 30.7734005649251", "", "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Criar Curvas
arcpy.gp.Contour_sa(REXTPr, curvas, Intervalo, "0", "1")
# Adicionar e Calcular Coluna Elevation
arcpy.AddField_management(curvas, "Elevation", "DOUBLE", "15", "2", "", "", "NULLABLE", "NON_REQUIRED", "")
# Process: Calculate Field (2)
arcpy.CalculateField_management(curvas, "Elevation", "[CONTOUR]", "VB", "")
# Process: Export to CAD (2)
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\CurvasNivel.shp", "DWG_R2010", CurvasDWG, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
# Apagar Arquivos Temporarios
arcpy.Delete_management(REXTPr, "RasterDataset")