# coding: utf-8

import os
import arcpy
import zipfile
import shutil
import time

import macro_conf

start_time = time.time()

arcpy.AddMessage(' ='*30)

# Variaveis Locais

# diretório raiz
UFC11 = r'C:\UFC\UFC11'

PAnalise = r'{0}\Shape\Exultorio.shp'.format(UFC11)
PPonto   = r'{0}\Dados\Ponto'.format(UFC11)

# Apagar arquivos e pasta existentes
if os.path.isfile(PAnalise):
	os.remove(PAnalise)
	arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(PAnalise))

if os.path.exists(PPonto):
	shutil.rmtree(PPonto, ignore_errors=True)
	arcpy.AddMessage(' + Removendo pasta: {0} - OK!'.format(PPonto))
	os.makedirs(PPonto)
	arcpy.AddMessage(' + Criando pasta: {0} - OK!'.format(PPonto))

# Extrair Arquivo Ponto.zip na Pasta Ponto.
ponto = zipfile.ZipFile('{0}\Dados\Ponto.zip'.format(UFC11))
ponto.extractall('{0}\Dados\Ponto'.format(UFC11))
ponto.close()
arcpy.AddMessage(' + Extraindo Arquivo {0}\Dados\Ponto.zip dentro da pasta {0}\Dados\Ponto'.format(UFC11))

# Pedi o Datum do Ponto. Pega o Primer Dado fornecido pelo usuario.
SCOR = arcpy.GetParameterAsText(0)
if SCOR == '#' or not SCOR:
	SCOR = "PROJCS['WGS_1984_UTM_Zone_24S',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',10000000.0],PARAMETER['Central_Meridian',-39.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]" # provide a default value if unspecified

# Local variables:
Ponto = r'{0}\Ponto.shp'.format(PPonto)

# Define a Projecao do Arquivo do Ponto.
arcpy.AddMessage(' + Definindo projecao do arquivo {0}'.format(Ponto))
arcpy.DefineProjection_management(Ponto, SCOR)

# Pega o Dado fornecido pelo usuario no campo Leste.
# leste = arcpy.GetParameterAsText(1)
leste = macro_conf.MACRO_CONFIG['p_exultorio_leste']

# Pega o Dado fornecido pelo usuario no campo Norte.
# norte = arcpy.GetParameterAsText(2)
norte = macro_conf.MACRO_CONFIG['p_exultorio_norte']

arcpy.AddMessage(' + Pontos fornecidos:')
arcpy.AddMessage(' + Campo Leste: {0}'.format(leste))
arcpy.AddMessage(' + Campo Norte: {0}'.format(norte))

# Inseri uma nova Fila no Shape Ponto
rowInserter = arcpy.InsertCursor(Ponto)

# Define as Cordenadas do Ponto
arcpy.AddMessage(' + Definindo coordenadas do arquivo {0}'.format(Ponto))
pointGeometry = arcpy.Point(leste, norte)

# Inseri o Ponto com as Cordenadas no Shape Ponto
newPoint = rowInserter.newRow()
newPoint.Shape = pointGeometry
rowInserter.insertRow(newPoint)

# Neste momento o arquivo Panalise é criado
# Projeta o Shape Ponto pra WGS84
arcpy.AddMessage(' + Projetando {0} no arquivo {1}'.format(Ponto, PAnalise))
try:
	arcpy.Project_management(Ponto, PAnalise, "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]", "", "PROJCS['WGS_1984_UTM_Zone_24S',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',10000000.0],PARAMETER['Central_Meridian',-39.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]")
except:
	arcpy.Project_management(Ponto, PAnalise, "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]", "", "PROJCS['WGS_1984_UTM_Zone_24S',GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]],PROJECTION['Transverse_Mercator'],PARAMETER['False_Easting',500000.0],PARAMETER['False_Northing',10000000.0],PARAMETER['Central_Meridian',-39.0],PARAMETER['Scale_Factor',0.9996],PARAMETER['Latitude_Of_Origin',0.0],UNIT['Meter',1.0]]")

arcpy.AddMessage(' + Arquivo Salvo: {0}'.format(PAnalise))

end_time = time.time()

run_time = end_time - start_time

arcpy.AddMessage(" + Tempo de execucao: {0}s".format(run_time))

arcpy.AddMessage(' ='*30)
