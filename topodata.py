# coding: utf-8

import arcpy
import os
import time
import shutil
import xlrd     # Biblioteca para leitura de arquivo no formato .xls
import urllib   # Biblioteca para download.
import zipfile  # Biblioteca para leitura de arquivos zipados.
from multiprocessing.pool import ThreadPool

import macro_conf

# Verificar as licencas
arcpy.CheckOutExtension('spatial')

start_time = time.time()

arcpy.AddMessage(' ='*30)

# dir raiz
UFC11 = r'C:\UFC\UFC11'

# Solicita no formulário o nível da Ottobacia
# OBCE = arcpy.GetParameterAsText(0)
OBCE = macro_conf.MACRO_CONFIG['nivel_ottobacia']

OBCE_VALIDS = ['1', '2', '3', '4', '5', '6']
if OBCE in OBCE_VALIDS:
    OBC = r'{0}\Dados\Nivel{1}.shp'.format(UFC11, OBCE)
    arcpy.AddMessage(' + Usando Nivel {0} para Ottobacias'.format(OBCE))
else:
    OBC = r'{0}\Dados\Nivel4.shp'.format(UFC11)
    arcpy.AddMessage(' + Usando Nivel 4 para Ottobacias'.format(OBCE))

# Ponto de Analise
PAnalise = arcpy.GetParameterAsText(1)
if PAnalise == '#' or not PAnalise:
    PAnalise = r'{0}\Dados\PAnalise.shp'.format(UFC11)
    if os.path.isfile(PAnalise):
        arcpy.AddMessage(' + Usando {0} como padrao para Ponto de Analise'.format(PAnalise))
        # bkp Panalise
        # necessario para rodar topodata.py mais de uma vez
        os.chdir(r'{0}\Dados'.format(UFC11))
        if not os.path.isfile(r'{0}\Dados\PAnalise_bkp.zip'.format(UFC11)):
            arcpy.AddMessage(' + Backup arquivos: {0} '.format(PAnalise))
            with zipfile.ZipFile('PAnalise_bkp.zip'.format(UFC11), 'w') as zip:
                bkp_files = ['PAnalise.shp', 'PAnalise.cpg', 'PAnalise.dbf',
                             'PAnalise.prj', 'PAnalise.sbn', 'PAnalise.sbx',
                             'PAnalise.shx', 'PAnalise.shp.xml']
                # writing each file one by one
                for bkp_file in bkp_files:
                    zip.write('{0}'.format(bkp_file))
    else:
        arcpy.AddMessage(' + Usando {0} como padrao para Ponto de Analise'.format(PAnalise))
        arcpy.AddMessage(' + Arquivo {0} nao existe.'.format(PAnalise))
        arcpy.AddMessage(' + Use 01 Ponto para criar o arquivo')
        arcpy.AddMessage(' + Saindo...')
        raise SystemExit

# descompactando bkp Panalise
os.chdir(r'{0}\Dados'.format(UFC11))
with zipfile.ZipFile('PAnalise_bkp.zip', 'r') as zip:
    # printing all the contents of the zip file
    # zip.printdir()
    # extracting all the files
    zip.extractall()

# Folga
# Folga = arcpy.GetParameterAsText(2)
Folga = macro_conf.MACRO_CONFIG['folga_km']
if Folga == '#' or not Folga:
    Folga = '0'
    arcpy.AddMessage(' + Usando valor {0} como padrao para Folga'.format(Folga))
else:
    arcpy.AddMessage(' + Usando valor {0} para Folga'.format(Folga))

# Poligono
Poligono = arcpy.GetParameterAsText(3)
if Poligono == '#' or not Poligono:
    # Se não passar valor para Poligono, nada vai acontecer.
    arcpy.AddMessage('')
else:
    # Se passar valor para Poligono, os niveis para Ottobacias
    # serao substituidos pelo valor informado para Poligono.
    OBC = Poligono
    arcpy.AddMessage(' + Usando {0} para Poligono'.format(Poligono))
    arcpy.AddMessage(' + Usando {0} como novo valor para Nivel de Ottobacia'.format(Poligono))

# Variaveis Locais
OBS    = r'{0}\Shape\sjoint.shp'.format(UFC11)
Grade  = r'{0}\Dados\GradeTopodata.shp'.format(UFC11)
Bacia  = r'{0}\Shape\Bacia.shp'.format(UFC11)
GTopo  = r'{0}\Shape\GTopo.shp'.format(UFC11)
Tabela = r'{0}\Shape\Tabela.xls'.format(UFC11)
Raster = r'{0}\Raster'.format(UFC11)

# Apagar Pastas
shutil.rmtree(Raster, ignore_errors=True)
arcpy.AddMessage(' + Removendo Pasta: {0} - OK!'.format(Raster))
if not os.path.isdir(Raster):
    os.makedirs(Raster)
    arcpy.AddMessage(' + Criando Pasta: {0} - OK!'.format(Raster))

dir_Shape = r'{0}\Shape'.format(UFC11)
shutil.rmtree(dir_Shape , ignore_errors=True)
try:
    arcpy.AddMessage(' + Removendo Pasta: {0} - OK!'.format(dir_Shape))
    time.sleep(3)
except:
    arcpy.AddMessage(' + Nao foi possivel remover {0} - OK!'.format(dir_Shape))
    arcpy.AddMessage(' + Dica: Feche o ArcGIS! =)')
os.makedirs(dir_Shape)
arcpy.AddMessage(' + Criando Pasta: {0} - OK!'.format(dir_Shape))

# Apagar arquivos
list_myfiles = [ OBS, GTopo, Bacia ]  # Tabela
for myfile in list_myfiles:
    if os.path.isfile(myfile):
        os.remove(myfile)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(myfile))
    else:
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(myfile))

# Adicionar Coluna bandeira no Ponto de Analise
arcpy.AddField_management(PAnalise, "Tipo", "SHORT", "4", "", "", "", "NULLABLE", "NON_REQUIRED", "")

# Calcular Coluna bandeira
arcpy.CalculateField_management(PAnalise, "Tipo", "1", "VB", "")

# Processo Uniao Espacial
arcpy.SpatialJoin_analysis(OBC, PAnalise, OBS, "JOIN_ONE_TO_ONE", "KEEP_ALL", "Tipo \"Tipo\" true true false 4 Short 0 4 , First, #, C:\\UFC\\UFC11\\Dados\\PAnalise.shp, Tipo, -1, -1", "INTERSECT", "0 Meters", "")

# Selecao da Bacia
arcpy.Select_analysis(OBS, Bacia, "\"Tipo\" = 1")

# Apagar Arquivos Anteriores
bfolga  = r'{0}\Shape\folg.shp'.format(UFC11)
if os.path.isfile(bfolga):
    os.remove(bfolga)
    arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(bfolga))

bfuniao = r'{0}\Shape\bfus.shp'.format(UFC11)
if os.path.isfile(bfuniao):
    os.remove(bfuniao)
    arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(bfuniao))

if float(Folga) > 0.0:

    # Valor da Folga
    Folga = '{0} Kilometers'.format(Folga)

    # Processo Buffer
    arcpy.Buffer_analysis(Bacia, bfolga, Folga, "FULL", "ROUND", "NONE", "")

    # Apagar Arquivo anterior
    if os.path.isfile(Bacia):
        os.remove(Bacia)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(Bacia))

    # Uniao Espacial da Bacia com Folga e o Nivel de Ottobacia
    arcpy.SpatialJoin_analysis(OBC, bfolga, bfuniao, "JOIN_ONE_TO_ONE", "KEEP_ALL", "Tipo \"Tipo\" true true false 4 Short 0 4 , First, #, C:\\UFC\\UFC11\\Shape\\folg.shp, Tipo, -1, -1", "INTERSECT", "0 Meters", "")

    # Selecao dos Poligonos Vizinhos
    arcpy.Select_analysis(bfuniao, Bacia, "\"Tipo\" > 0")

    # Apagar Arquivos Temporarios
    if os.path.isfile(bfolga):
        os.remove(bfolga)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(bfolga))

    if os.path.isfile(bfuniao):
        os.remove(bfuniao)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(bfuniao))

# Processo Cortar Grade
arcpy.Clip_analysis(Grade, Bacia, GTopo, '')

# Exportar Tabela
arcpy.TableToExcel_conversion(GTopo, Tabela, 'NAME', 'CODE')

# Apagar Coluna bandeira
arcpy.DeleteField_management(PAnalise, 'Tipo')

# Apagar Arquivos
if os.path.isfile(OBS):
    os.remove(OBS)
    arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(OBS))

if os.path.isfile(GTopo):
    os.remove(GTopo)
    arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(GTopo))

workbook = xlrd.open_workbook(r'{0}\Shape\Tabela.xls'.format(UFC11))  # Abre a tabela.

worksheet = workbook.sheet_by_name('Tabela')  # Busca a aba de nome Tabela.

arcpy.AddMessage(' + Baixando Arquivos do Servidor TOPODATA')

urls_list = []

# Leitura das linhas preenchidas na aba Tabela.
for row_num in xrange(worksheet.nrows):

    if row_num == 0:  # Para primeira linha.
        lista = worksheet.row_values(row_num)  # Lista dos valores da primeira linha.
        i = 0
        while i < len(lista):
            if lista[i] == 'URL':  # Verifica a coluna de valor URL
                URL = i
            if lista[i] == 'TOPOID':  # Verifica a coluna de valor TOPOID
                TOPOID = i
            i = i + 1
        continue

    row = worksheet.row_values(row_num)  # Lista dos valores da linha.
    url = row[URL]  # Coluna de valores do URL
    local_filename = '{0}\Topodata\{1}ZN.zip'.format(UFC11, row[TOPOID])  # Coluna de valores do TOPOID

    if os.path.isfile(local_filename):  # Verifica a existencia do arquivo.
        arcpy.AddMessage(' + Arquivo ja existe: {0}'.format(local_filename))
        zf = zipfile.ZipFile(local_filename, 'r')  # Leitura do arquivo zipado.
        zf.extractall('{0}\RASTER'.format(UFC11))  # Pasta extracao do arquivo zipado.
        arcpy.AddMessage(' + Extraindo {0} para pasta {1}\RASTER'.format(local_filename, UFC11))
    else:
        urls_list.append(url)

if urls_list:

    arcpy.AddMessage(' + URLS para downloads:')

    for url in urls_list:
        arcpy.AddMessage(' + {0}'.format(url))

    def download_file(url):
        local_filename = '{0}\Topodata\{1}'.format(UFC11, url.split('/')[-1])
        arcpy.AddMessage(' + Baixando para: {0}'.format(local_filename))
        urllib.urlretrieve(url, local_filename)
        return local_filename

    n_pools = len(urls_list)

    results = ThreadPool(n_pools).imap_unordered(download_file, urls_list)

    for i in results:
        arcpy.AddMessage(' + Arquivo baixado: {0}'.format(i))

    def unzip_file(url):
        local_filename = '{0}\Topodata\{1}'.format(UFC11, url.split('/')[-1])
        zf = zipfile.ZipFile(local_filename, 'r')  # Leitura do arquivo zipado.
        zf.extractall('{0}\RASTER'.format(UFC11))  # Pasta extracao do arquivo zipado.
        arcpy.AddMessage(' + Extraindo {0} para pasta {1}\RASTER'.format(local_filename, UFC11))
        return local_filename

    results = ThreadPool(n_pools).imap_unordered(unzip_file, urls_list)

    for i in results:
        arcpy.AddMessage(' + Arquivo extraido: {0}'.format(i))

# Variavel para a pasta dos raster
# pasta = r'{0}\Raster'.format(UFC11)   # acho que este dir ja foi declarado

# Lista com os arquivos rasters
rasters = []

# Vetor com todos os arquivos da Pasta
arquivos = os.walk(Raster)

# Cria uma lista dos arquivos da Pasta Raster
for root, dirs, files in arquivos:
    for arquivo in files:
        (nomearquivo, extensao) = os.path.splitext(arquivo)
        rasters.append(nomearquivo + extensao)

# Conta o numero do arquivos da pasta Raster
nro = len(rasters)
arcpy.AddMessage(' + Numero de rasters: {0}'.format(nro))
arcpy.AddMessage(' + Rasters dentro pasta {0}: {1}'.format(r'{0}\Raster'.format(UFC11), rasters))

# Para o numero de arquivos 1
if nro == 1:

    # Muda o nome do arquivo para raster.tif.
    arcpy.AddMessage(' + Processando Uniao do Mosaico de Imagens: 1/1')  # fake
    os.rename( r'{0}\Raster\{1}'.format(UFC11, rasters[0]), r'{0}\Raster\raster.tif'.format(UFC11))

elif nro == 2:

    os.rename( r'{0}\Raster\{1}'.format(UFC11, rasters[0]), r'{0}\Raster\raster1.tif'.format(UFC11))
    os.rename( r'{0}\Raster\{1}'.format(UFC11, rasters[1]), r'{0}\Raster\raster2.tif'.format(UFC11))

    Raster  = r'{0}\Raster'.format(UFC11)
    raster1 = r'{0}\raster1.tif'.format(Raster)
    raster2 = r'{0}\raster2.tif'.format(Raster)

    arcpy.AddMessage(' + Processando Uniao do Mosaico de Imagens: 1/1')  # fake
    arcpy.MosaicToNewRaster_management('{0} ; {1}'.format(raster1, raster2), Raster, 'raster.tif', '', '32_BIT_FLOAT', '', '1', 'LAST', 'FIRST')

    time.sleep(3)

    if os.path.isfile(raster1):
        os.remove(raster1)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(raster1))

    time.sleep(3)

    if os.path.isfile(raster2):
        os.remove(raster2)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(raster2))

else:

    arcpy.AddMessage(' + Processando Uniao do Mosaico de Imagens: 1/{0}'.format(nro))  # fake
    os.rename( r'{0}\Raster\{1}'.format(UFC11, rasters[0]), r'{0}\Raster\raster1.tif'.format(UFC11))
    os.rename( r'{0}\Raster\{1}'.format(UFC11, rasters[1]), r'{0}\Raster\raster2.tif'.format(UFC11))

    # Local variables:
    Raster  = r'{0}\Raster'.format(UFC11)
    raster1 = r'{0}\raster1.tif'.format(Raster)
    raster2 = r'{0}\raster2.tif'.format(Raster)
    raster  = r'{0}\raster.tif'.format(Raster)

    for i in range(2, nro):

        # Process: Mosaic To New Raster
        arcpy.AddMessage(' + Processando Uniao do Mosaico de Imagens: {0}/{1}'.format(i, nro))
        arcpy.MosaicToNewRaster_management('{0} ; {1}'.format(raster1, raster2), Raster, 'raster.tif', '', '32_BIT_FLOAT', '', '1', 'LAST', 'FIRST')

        time.sleep(3)

        if os.path.isfile(raster1):
            os.remove(raster1)
            arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(raster1))

        time.sleep(3)

        if os.path.isfile(raster2):
            os.remove(raster2)
            arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(raster2))

        time.sleep(3)

        # Process: Rename raster to raster1
        os.rename( r'{0}'.format(raster), r'{0}'.format(raster1) )
        os.rename( r'{0}\Raster\{1}'.format(UFC11, rasters[i]), r'{0}'.format(raster2) )

    arcpy.AddMessage(' + Processando Uniao do Mosaico de Imagens: {0}/{1}'.format(i + 1, nro))
    # Uniao Raster Final.
    arcpy.MosaicToNewRaster_management('{0} ; {1}'.format(raster1, raster2), Raster, 'raster.tif', '', '32_BIT_FLOAT', '', '1', 'LAST', 'FIRST')

    if os.path.isfile(raster1):
        os.remove(raster1)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(raster1))

    if os.path.isfile(raster2):
        os.remove(raster2)
        arcpy.AddMessage(' + Removendo Arquivo: {0} - OK!'.format(raster2))

# Pegar os dados pelo usuario
RCortar = r'{0}\Raster\raster.tif'.format(UFC11) # provide a default value if unspecified

# Variaveis Locais
REXT = r'{0}\Raster\REXT.tif'.format(UFC11)

# Process: Define Projection
arcpy.AddMessage(' + Projetando Arquivo Raster')
arcpy.DefineProjection_management(RCortar, "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")

# Process: Extract by Mask
arcpy.AddMessage(' + Cortando Arquivo Raster')
arcpy.gp.ExtractByMask_sa(RCortar, Bacia, REXT)

# Apagar Arquivo Raster Mosaico
arcpy.Delete_management(RCortar, 'RasterDataset')

# Apagar arquivos existentes
arcpy.AddMessage(' + Apagando Arquivos Temporarios')
arcpy.Delete_management(OBS, 'ShapeFile')
arcpy.Delete_management(GTopo, 'ShapeFile')
arcpy.Delete_management(Bacia, 'ShapeFile')
arcpy.Delete_management(Tabela, 'File')

dir_Shape = r'{0}\Shape'.format(UFC11)
shutil.rmtree(dir_Shape , ignore_errors=True)
arcpy.AddMessage(' + Removendo Pasta: {0} - OK!'.format(dir_Shape))
os.makedirs(dir_Shape)

end_time = time.time()

run_time = end_time - start_time

arcpy.AddMessage(" + Tempo de execucao: {0}s".format(run_time))

arcpy.AddMessage(' ='*30)
