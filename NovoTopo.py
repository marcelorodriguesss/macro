# Import arcpy module
import arcpy
# Script arguments
Pasta = arcpy.GetParameterAsText(0)
if Pasta == '#' or not Pasta:
    Pasta = "Bacia" # provide a default value if unspecified
# Local variables:
Bacia = Pasta
Saida = "C:\\UFC\\UFC11\\Saida"
# Arquivos Originais
B3DTOPO = "C:\\UFC\\UFC11\\Saida\\B3DTOPO.DWG"
CNivelTOPOUTM = "C:\\UFC\\UFC11\\Saida\\CNivelTOPOUTM.DWG"
PPluvioTopo = "C:\\UFC\\UFC11\\Saida\\PPluvioTopo.DWG"
RTOPO = "C:\\UFC\\UFC11\\Saida\\RTOP.dwg"
Talvegue3DTOPO = "C:\\UFC\\UFC11\\Saida\\CanalPral3DTOPO.dwg"
ThiessenTopo = "C:\\UFC\\UFC11\\Saida\\ThiessenTopo.dwg"
btopoutm = "C:\\UFC\\UFC11\\Saida\\btopoutm.shp"
pbhi = "C:\\UFC\\UFC11\\Shape\\pbhidro.shp"
CNUTM = "C:\\UFC\\UFC11\\Saida\\CNUTM.shp"
PPluvioTOPO_shp = "C:\\UFC\\UFC11\\Saida\\PPluvioTOPO.shp"
Rtputm = "C:\UFC\UFC11\Shape\Rtputm.shp"
ThiessenTPUTM_shp = "C:\\UFC\\UFC11\\Saida\\ThiessenTPUTM.shp"
TPralT = "C:\\UFC\\UFC11\\Saida\\CanalPral.shp"
vtopoutm_shp = "C:\\UFC\\UFC11\\Saida\\vtopoutm.shp"
Estacoes_xls = "C:\\UFC\\UFC11\\Saida\\Estacoes.xls"
Rastopoutm_tif = "C:\\UFC\\UFC11\\Raster\\Rastopoutm.tif"
rtopo_tif = "C:\\UFC\\UFC11\\Raster\\rtopo.tif"
bhidro_shp = "C:\\UFC\\UFC11\\Shape\\bhidro.shp"
BTopo_shp = "C:\\UFC\\UFC11\\Shape\\BTopo.shp"
cnivel_shp = "C:\\UFC\\UFC11\\Shape\\cnivel.shp"
r2dtopo_shp = "C:\\UFC\\UFC11\\Shape\\r2dtopo.shp"
talvegueTD_shp = "C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp"
vtopo_shp = "C:\\UFC\\UFC11\\Shape\\vtopo.shp"
Bacia3DTopo_shp = "C:\\UFC\\UFC11\\Saida\\Bacia3DTopo.shp"
Talvegue3DTOPO_shp = "C:\\UFC\\UFC11\\Saida\\CanalPral3DTOPO.shp"
# Arquivos Criados (Movidos)
Bacia3DTopo = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\Bacia3DTopo.dwg"
Cnivel = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\Cnivel.dwg"
ppluvio = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\ppluvio.dwg"
rtop = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\rtop.dwg"
talvegue3D = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\CanalPral3D.dwg"
thiessen = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\thiessen.dwg"
btoputm = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\btopoutm.shp"
Cnivel_shp__2_ = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\Cnivel.shp"
ppluviot_shp = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\ppluviomt.shp"
riostopoutm = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\rtopoutm.shp"
thiessen_shp__2_ = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\thiessen.shp"
talveguepral_shp = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\CanalPral.shp"
Estacoes_xls__2_ = "C:\\UFC\\UFC11\\Saida\\" + Bacia + "\\Estacoes.xls"
# Processo: Criar Pasta
arcpy.AddMessage("Criando Nova Pasta")
arcpy.CreateFolder_management(Saida, Pasta)
# Processos: Copiar Arquivos (Mover)
arcpy.AddMessage("Copiando Arquivos")
arcpy.Copy_management(B3DTOPO, Bacia3DTopo, "CadDrawingDataset")
arcpy.Copy_management(CNivelTOPOUTM, Cnivel, "CadDrawingDataset")
arcpy.Copy_management(PPluvioTopo, ppluvio, "CadDrawingDataset")
arcpy.Copy_management(RTOPO, rtop, "CadDrawingDataset")
arcpy.Copy_management(Talvegue3DTOPO, talvegue3D, "CadDrawingDataset")
arcpy.Copy_management(ThiessenTopo, thiessen, "CadDrawingDataset")
arcpy.Copy_management(btopoutm, btoputm, "ShapeFile")
arcpy.Copy_management(CNUTM, Cnivel_shp__2_, "ShapeFile")
arcpy.Copy_management(PPluvioTOPO_shp, ppluviot_shp, "ShapeFile")
arcpy.Copy_management(Rtputm, riostopoutm, "ShapeFile")
arcpy.Copy_management(ThiessenTPUTM_shp, thiessen_shp__2_, "ShapeFile")
arcpy.Copy_management(TPralT, talveguepral_shp, "ShapeFile")
arcpy.Copy_management(Estacoes_xls, Estacoes_xls__2_, "File")
# Processo: Apagar Arquivos Anteriores
arcpy.AddMessage("Apagando Arquivos Anteriores")
arcpy.Delete_management(CNivelTOPOUTM, "CadDrawingDataset")
arcpy.Delete_management(PPluvioTopo, "CadDrawingDataset")
arcpy.Delete_management(RTOPO, "CadDrawingDataset")
arcpy.Delete_management(Talvegue3DTOPO, "CadDrawingDataset")
arcpy.Delete_management(ThiessenTopo, "CadDrawingDataset")
arcpy.Delete_management(btopoutm, "ShapeFile")
arcpy.Delete_management(CNUTM, "ShapeFile")
arcpy.Delete_management(PPluvioTOPO_shp, "ShapeFile")
arcpy.Delete_management(B3DTOPO, "CadDrawingDataset")
arcpy.Delete_management(Rtputm, "ShapeFile")
arcpy.Delete_management(ThiessenTPUTM_shp, "ShapeFile")
arcpy.Delete_management(pbhi, "ShapeFile")
arcpy.Delete_management(TPralT, "ShapeFile")
arcpy.Delete_management(Estacoes_xls, "File")
arcpy.Delete_management(vtopoutm_shp, "ShapeFile")
arcpy.Delete_management(vtopo_shp, "ShapeFile")
arcpy.Delete_management(talvegueTD_shp, "ShapeFile")
arcpy.Delete_management(r2dtopo_shp, "ShapeFile")
arcpy.Delete_management(cnivel_shp, "ShapeFile")
arcpy.Delete_management(BTopo_shp, "ShapeFile")
arcpy.Delete_management(bhidro_shp, "ShapeFile")
arcpy.Delete_management(rtopo_tif, "RasterDataset")
arcpy.Delete_management(Rastopoutm_tif, "RasterDataset")
arcpy.Delete_management(Bacia3DTopo_shp, "ShapeFile")
arcpy.Delete_management(Talvegue3DTOPO_shp, "ShapeFile")