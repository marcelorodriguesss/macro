# Importar Modulo arcpy
import arcpy
# Verificar as licencias
arcpy.CheckOutExtension("spatial")
arcpy.CheckOutExtension("3D")
# Pegar o Arquivo Raster anterior
Raster = "C:\UFC\UFC11\Raster\REXT.tif"
# Variaveis Locais
TRel = "C:\UFC\UFC11\Raster\emrrel.tif"
FD = "C:\UFC\UFC11\Raster\FD.tif"
FA = "C:\UFC\UFC11\Raster\FA.tif"
RC = "C:\UFC\UFC11\Raster\mcal.tif"
SL = "C:\UFC\UFC11\Raster\slink.tif"
SO = "C:\UFC\UFC11\Raster\sorder.tif"
Rios = "C:\UFC\UFC11\Shape\Rios2Dt.shp"
Rios2D = "C:\UFC\UFC11\Saida\Rios2D.shp"
Shape = "C:\UFC\UFC11\Shape"
rtemp = "C:\UFC\UFC11\Shape\prtemp.shp"
Rios2DUTM = "C:\\UFC\\UFC11\\Saida\\Rios2DUTM.shp"
# Apagar arquivos anteriores
arcpy.AddMessage("Apagando Arquivos Anteriores")
arcpy.Delete_management(TRel, "RasterDataset")
arcpy.Delete_management(FD, "RasterDataset")
arcpy.Delete_management(FA, "RasterDataset")
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(Rios2D, "ShapeFile")
arcpy.Delete_management(Rios2DUTM, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")
# Encher os Buracos do DEM
arcpy.AddMessage("Prenchendo os Buracos do DEM")
arcpy.gp.Fill_sa(Raster, TRel, "")
# Obter o Flow Direction do DEM
arcpy.AddMessage("Calculo da Direcao do Fluxo do DEM")
arcpy.gp.FlowDirection_sa(TRel, FD, "NORMAL", "")
# Obter o Flow Accumulation do DEM
arcpy.AddMessage("Calculo do Fluxo Acumulado do DEM")
arcpy.gp.FlowAccumulation_sa(FD, FA, "", "FLOAT")
# Clasificar os Rios pela quantidade de area drenada
# Importar a Livraria Spatial Analyst
from arcpy.sa import *
# Definir o Poligono
polig = "C:\UFC\UFC11\Saida\Pol.shp"
Projecao = "C:\UFC\UFC11\Saida\Pol.prj"
# Adicionar Coluna AreaBH e Perimetro
arcpy.AddField_management(polig, "AreaBH", "DOUBLE", "15", "4", "", "", "NULLABLE", "NON_REQUIRED", "")
# Adicionar Propriedades da Geometria da Area
arcpy.AddGeometryAttributes_management(polig, "AREA;PERIMETER_LENGTH", "METERS", "SQUARE_KILOMETERS", Projecao)
# Calcular Area do Poligono
arcpy.CalculateField_management(polig, "AreaBH", "[POLY_AREA]", "VB", "")
# Limitar os Rios para 5/1000 da Area Total
arcpy.AddMessage("Limitando os Rios para 1/2000 da Area Total")
# Variavel para texto da Area
ABH = "C:\\UFC\\UFC11\\Shape\\ABH.txt"
# Apagar arquivos anteriores
arcpy.Delete_management(ABH, "File")
# Exportar a Arquivo txt
arcpy.ExportXYv_stats(polig, "AreaBH", "COMMA", ABH, "NO_FIELD_NAMES")
# Abrir Arquivo da Area da Bacia Hidraulica
arqv=open('C:\UFC\UFC11\Shape\ABH.txt','r')
# Ler fila por fila
lerfil= arqv.readlines()
# Fechar Arquivo
arqv.close()
# Iniciar Vetor dos Pontos Iniciais
vareabh = []
# Carregar Vetor Pontos Inicias
for vetor in lerfil:
    # Cortar em Virgula
    line = vetor.split(',')
    # Carregar Ponto Inicial
    vareabh += [line[2]]
# Numero de Celulas
ambah = int((float(vareabh[0])*1000000/(900))*0.0005)
if ambah > 4:
	# Indicar o valor minimo do FA
    acum = ambah
else:
	# Indicar o valor minimo do FA
    acum = 4
# Apagar arquivos temporarios
arcpy.Delete_management(ABH, "File")
# Condicao do Raster
outRaster = Raster(FA) > acum
# Salvar o Raster
outRaster.save(RC)
# Unir os Pontos continuos dos Rios
arcpy.gp.StreamLink_sa(RC, FD, SL)
# Definir a Ordem dos Rios
arcpy.AddMessage("Clasificando Rios")
arcpy.gp.StreamOrder_sa(SL, FD, SO, "STRAHLER")
# Exportar os rios a Shapefile
arcpy.AddMessage("Exportando Rios para Shapefile")
arcpy.gp.StreamToFeature_sa(SO, FD, Rios, "SIMPLIFY")
# Criar Shapefile com Propiedades Ativas de Cotas
arcpy.CreateFeatureclass_management(Shape, "prtemp.shp", "POLYLINE", "", "ENABLED", "ENABLED", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119522E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")
# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\\UFC\\UFC11\\Shape\\prtemp.shp;C:\\UFC\\UFC11\\Shape\\Rios2Dt.shp", Rios2D, "")
# Projetar Shapefile Rios
arcpy.Project_management(Rios2D, Rios2DUTM, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Apagar Arquivos Temporais
arcpy.AddMessage("Apagando Arquivos Temporarios")
arcpy.Delete_management(TRel, "RasterDataset")
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")
# Verificar as licencias
arcpy.CheckInExtension("Spatial")
# Visualizar os arquivos resultados
# Importar Livraria mapping
import arcpy.mapping
# Definir os Arquivos Shape a inserir
arquivo1 = r"C:\UFC\UFC11\Saida\Pol.shp"
arquivo2 = r"C:\UFC\UFC11\Saida\Rios2D.shp"
arquivo3 = r"C:\UFC\UFC11\Saida\Pontos.shp"
# Definir o Arquivo de ArcGIS atual.
tatual = arcpy.mapping.MapDocument("Current")
# Definir o Layer Atual
camadaatual = arcpy.mapping.ListDataFrames(tatual,"*")[0]
# Criar uma Nova Camada
camada1 = arcpy.mapping.Layer(arquivo1)
camada2 = arcpy.mapping.Layer(arquivo2)
camada3 = arcpy.mapping.Layer(arquivo3)
# Adicionar a Nova Camada no arquivo atual
arcpy.AddMessage("Adicionando Arquivos na Tela Atual")
arcpy.mapping.AddLayer(camadaatual, camada3,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada2,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada1,"Bottom")
# Atualizar los Arquivos
arcpy.RefreshActiveView()
arcpy.RefreshTOC()
# Zoom Shapefile Pol.shp
# Escolher o Documento Atual
mxd = arcpy.mapping.MapDocument('current')
# Escolher o Layer Atual
df = arcpy.mapping.ListDataFrames(mxd)[0]
# Escolher o Shape do Poligono
lyr = arcpy.mapping.ListLayers(mxd, 'Pol*', df)[0]
# Definir a Variavel pra fazer Zoom
ext = lyr.getExtent()
# Fazer Zoom to Layer
df.extent = ext