# coding: utf-8

import arcpy

import math
import xlrd
import arcpy.mapping
import time
import os
import zipfile
from shutil import copy2

# import macro_conf

# Verificar as licencias
arcpy.CheckOutExtension("spatial")
arcpy.CheckOutExtension("3D")

# Pegar os Parametros do usuario
# Exutorio = arcpy.GetParameterAsText(0)
# if Exutorio == '#' or not Exutorio:
#     Exut = "C:\UFC\UFC11\Shape\Exutorio.shp" # valor pelo default
# else:
#     Exut = "C:\UFC\UFC11\Shape\Exutorio.shp"
#     arcpy.Delete_management(Exut, "ShapeFile")
#     arcpy.Copy_management(Exutorio, Exut, "ShapeFile")

# Exut = macro_conf.MACRO_CONFIG['shp_exultorio']

Exut = 'C:\UFC\UFC11\Shape\Exutorio.shp'

if not os.path.isfile(Exut):
    arcpy.AddMessage(u' + Arquivo shape não encontrado: {0}'.format(Exut))
    raise SystemExit

dirname = os.path.dirname(Exut)

os.chdir(dirname)

name_exutshp = os.path.basename(Exut).split('.')[0]

if not os.path.isfile('{0}_bkp.zip'.format(name_exutshp)):
    arcpy.AddMessage(' + Backup arquivo shape: {0}'.format(Exut))
    with zipfile.ZipFile('{0}_bkp.zip'.format(name_exutshp), 'w') as zip:
        bkp_files = ['{0}.shp'.format(name_exutshp), '{0}.cpg'.format(name_exutshp),
                     '{0}.dbf'.format(name_exutshp), '{0}.prj'.format(name_exutshp),
                     '{0}.sbn'.format(name_exutshp), '{0}.sbx'.format(name_exutshp),
                     '{0}.shx'.format(name_exutshp)]
        for bkp_file in bkp_files:
            zip.write('{0}'.format(bkp_file))
else:
    arcpy.AddMessage(' + Descompactando: {0}_bkp.zip'.format(name_exutshp))
    with zipfile.ZipFile('{0}_bkp.zip'.format(name_exutshp), 'r') as zip:
        zip.printdir()
        zip.extractall()

Inter = arcpy.GetParameterAsText(1)
if Inter == '#' or not Inter:
	Inter = "10" # valor pelo default

# Quantidade Minima de Anos dos Postos Pluviometricos
Anos = arcpy.GetParameterAsText(2)
if Anos == '#' or not Anos:
    Anos = "23" # valor pelo default

Projecao = "C:\UFC\UFC11\Dados\Ponto\Ponto.prj"

# Variaveis Locais
# Arquivos dos Processos Anteriores
FD = "C:\UFC\UFC11\Raster\FD.tif"
FA = "C:\UFC\UFC11\Raster\FA.tif"
VRios = "C:\\UFC\\UFC11\\Shape\\VRios.shp"
Rios2d = "C:\\UFC\\UFC11\\Shape\\Rios2D.shp"
REXT = "C:\\UFC\\UFC11\\Raster\\REXT.tif"
EPluvioi = "C:\UFC\UFC11\Dados\EPluvio.shp"
# Arquivos Temporarios
EPluvio = "C:\UFC\UFC11\Dados\EPT.shp"
FAR = "C:\UFC\UFC11\Raster\FAR.tif"
BH = "C:\UFC\UFC11\Raster\BH.tif"
pchtemp = "C:\\UFC\\UFC11\\Shape\\pchtemp.shp"
bhtemp = "C:\UFC\UFC11\Shape\mabhidrotemp.shp"
Shape = "C:\\UFC\\UFC11\\Shape"
politemp = "C:\\UFC\\UFC11\\Shape\\politemp.shp"
BTopotemp = "C:\UFC\UFC11\Shape\BTopotemp.shp"
rtop = "C:\UFC\UFC11\Raster\mbrtop.tif"
cniveltemp = "C:\UFC\UFC11\Shape\cniveltemp.shp"
linhawgs84 = "C:\\UFC\\UFC11\\Shape\\linhawgs84.shp"
btopobuf = "C:\UFC\UFC11\Saida\pbtopobuf.shp"
PPluv = "C:\UFC\UFC11\Saida\PPluv.shp"
thtem = "C:\UFC\UFC11\Shape\pthtem.shp"
thtemp = "C:\UFC\UFC11\Shape\pthtemp.shp"
ttemp = "C:\UFC\UFC11\Shape\pttemp.shp"
ptem = "C:\UFC\UFC11\Shape\ptem.shp"
ptemp = "C:\UFC\UFC11\Shape\ptemp.shp"
thies = "C:\UFC\UFC11\Shape\pthies.shp"
TTOPOUTM = "C:\UFC\UFC11\Shape\ThiessenTOPOUTM.shp"
bproj = "C:\UFC\UFC11\Shape\pbproj.shp"
ptosor = "C:\\UFC\\UFC11\\Shape\\ptsor.shp"

# Arquivos Resultados
Bacia = "C:\UFC\UFC11\Shape\pbhidro.shp"
r2dtutm = "C:\UFC\UFC11\Shape\Rtputm.shp"
pvert = "C:\\UFC\\UFC11\\Shape\\pvert.shp"
r3Dt = "C:\UFC\UFC11\Shape\R3Dtopo.shp"
PtosT = "C:\UFC\UFC11\Shape\PtosT.shp"
BTopo = "C:\UFC\UFC11\Shape\BTopo.shp"
rtopo = "C:\UFC\UFC11\Raster\drtopo.tif"
cnivel = "C:\UFC\UFC11\Shape\cnivel.shp"
PPTOPO = "C:\UFC\UFC11\Saida\PPluvioTOPO.shp"
ThiessenTPUTM = "C:\UFC\UFC11\Saida\ThiessenTPUTM.shp"
Estacoes = "C:\UFC\UFC11\Saida\Estacoes.xls"
drios = "C:\\UFC\\UFC11\\Shape\\drios.dbf"

# Apagar arquivos anteriores
arcpy.AddMessage("Apagando Arquivos Anteriores")
arcpy.Delete_management(BH, "RasterDataset")
arcpy.Delete_management(pchtemp, "ShapeFile")
arcpy.Delete_management(bhtemp, "ShapeFile")
arcpy.Delete_management(politemp, "ShapeFile")
arcpy.Delete_management(EPluvio, "ShapeFile")
arcpy.Delete_management(Bacia, "ShapeFile")
arcpy.Delete_management(r2dtutm, "ShapeFile")
arcpy.Delete_management(r3Dt, "ShapeFile")
arcpy.Delete_management(PtosT, "ShapeFile")
arcpy.Delete_management(ptosor, "ShapeFile")
arcpy.Delete_management(pvert, "ShapeFile")
arcpy.Delete_management(BTopotemp, "ShapeFile")
arcpy.Delete_management(BTopo, "ShapeFile")
arcpy.Delete_management(rtop, "RasterDataset")
arcpy.Delete_management(rtopo, "RasterDataset")
arcpy.Delete_management(cniveltemp, "ShapeFile")
arcpy.Delete_management(cnivel, "ShapeFile")
arcpy.Delete_management(linhawgs84, "ShapeFile")
arcpy.Delete_management(btopobuf, "ShapeFile")
arcpy.Delete_management(PPluv, "ShapeFile")
arcpy.Delete_management(thtem, "ShapeFile")
arcpy.Delete_management(thtemp, "ShapeFile")
arcpy.Delete_management(ttemp, "ShapeFile")
arcpy.Delete_management(ptem, "ShapeFile")
arcpy.Delete_management(ptemp, "ShapeFile")
arcpy.Delete_management(PPTOPO, "ShapeFile")
arcpy.Delete_management(thies, "ShapeFile")
arcpy.Delete_management(TTOPOUTM, "ShapeFile")
arcpy.Delete_management(bproj, "ShapeFile")
arcpy.Delete_management(ThiessenTPUTM, "ShapeFile")
arcpy.Delete_management(Estacoes, "File")
arcpy.Delete_management(drios, "DbaseTable")

# Criar Shapefile com Propiedades Ativas de Cotas
arcpy.CreateFeatureclass_management(Shape, "politemp.shp", "POLYGON", "", "ENABLED", "ENABLED", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119522E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")
# Definir a Bacia do Ponto Exutorio
arcpy.AddMessage("Criando Bacia do Exutorio")
arcpy.gp.Watershed_sa(FD, Exut, BH, "FID")
# Convertir o Raster da Bacia a Poligono
arcpy.RasterToPolygon_conversion(BH, bhtemp, "SIMPLIFY", "VALUE")
# Local variables:
phidrtemp = "C:\\UFC\\UFC11\\Shape\\phidrotemp.shp"
pshtemp = "C:\\UFC\\UFC11\\Shape\\pshtemp.shp"
# Apagar arquivos Anteriores
arcpy.Delete_management(phidrtemp, "ShapeFile")
arcpy.Delete_management(pshtemp, "ShapeFile")
# Adicionar e Calcular Coluna PBacia
arcpy.AddField_management(Exut, "PBacia", "LONG", "5", "", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(Exut, "PBacia", "1", "VB", "")
# Uniao Espacial do Exutorio e a Bacia
arcpy.SpatialJoin_analysis(bhtemp, Exut, phidrtemp, "JOIN_ONE_TO_ONE", "KEEP_ALL", "ID \"ID\" true true false 10 Double 0 10 ,First,#,C:\\UFC\\UFC11\\Shape\\mabhidrotemp.shp,ID,-1,-1;GRIDCODE \"GRIDCODE\" true true false 10 Double 0 10 ,First,#,C:\\UFC\\UFC11\\Shape\\mabhidrotemp.shp,GRIDCODE,-1,-1;Id_1 \"Id_1\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,Id,-1,-1;ARCID \"ARCID\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,ARCID,-1,-1;GRID_CODE \"GRID_CODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,GRID_CODE,-1,-1;FROM_NODE \"FROM_NODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,FROM_NODE,-1,-1;TO_NODE \"TO_NODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,TO_NODE,-1,-1;ORIG_FID \"ORIG_FID\" true true false 9 Long 0 9 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,ORIG_FID,-1,-1;drtopo \"drtopo\" true true false 13 Float 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,drtopo,-1,-1;drtopo_1 \"drtopo_1\" true true false 13 Float 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,drtopo_1,-1,-1;PBacia \"PBacia\" true true false 0 Long 5 10 ,First,#,C:\\UFC\\UFC11\\Shape\\Exutorio.shp,PBacia,-1,-1", "INTERSECT", "60 Meters", "")
# Selecao do Poligono da Bacia
arcpy.Select_analysis(phidrtemp, pshtemp, "\"PBacia\" =1")
# Apagar arquivos Temporarios
arcpy.Delete_management(bhtemp, "ShapeFile")
arcpy.Delete_management(phidrtemp, "ShapeFile")
# Renomear Arquivo
arcpy.Rename_management(pshtemp, bhtemp, "ShapeFile")
# Dissolver e Criar um so Poligono
arcpy.Dissolve_management(bhtemp, pchtemp, "GRIDCODE", "", "MULTI_PART", "DISSOLVE_LINES")
# Apagar Arquivo Anterior
arcpy.Delete_management(bhtemp, "ShapeFile")
# Renomear Arquivo
arcpy.Rename_management(pchtemp, bhtemp, "")
# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\UFC\UFC11\Shape\politemp.shp;C:\UFC\UFC11\Shape\mabhidrotemp.shp", Bacia, "ID \"ID\" true true false 0 Long 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\bhidrotemp.shp,ID,-1,-1;GRIDCODE \"GRIDCODE\" true true false 0 Long 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\bhidrotemp.shp,GRIDCODE,-1,-1")
# Adicionar Coluna no Poligono da Bacia e Calcular Valor
arcpy.AddField_management(Bacia, "BACIA", "SHORT", "3", "", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(Bacia, "BACIA", "1", "VB", "")
# Projetar a Bacia
arcpy.Project_management(Bacia, bproj, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Adicionar Coluna AreaBH e Perimetro
arcpy.AddField_management(bproj, "AreaBH", "DOUBLE", "15", "4", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.AddField_management(bproj, "PerimBH", "LONG", "50", "", "", "", "NULLABLE", "NON_REQUIRED", "")
# Adicionar Propriedades da Geometria da Area e Perimetro da Bacia
arcpy.AddGeometryAttributes_management(bproj, "AREA;PERIMETER_LENGTH", "METERS", "SQUARE_KILOMETERS", Projecao)
# Calcular Area da Bacia Hidraulica
arcpy.CalculateField_management(bproj, "AreaBH", "[POLY_AREA]", "VB", "")
# Calcular Perimetro da Bacia Hidraulica
arcpy.CalculateField_management(bproj, "PerimBH", "[PERIMETER]", "VB", "")
# Apagar Colunas Gridcode e PolyArea e Perimeter
arcpy.DeleteField_management(bproj, "GRIDCODE;POLY_AREA;PERIMETER")
# Limitar os Rios para 2/1000 da Area Total
arcpy.AddMessage("Limitando os Rios para 1/2000 da Area Total")
# Variavel para texto da Area
ABH = "C:\\UFC\\UFC11\\Shape\\ABH.txt"
# Apagar arquivos anteriores
arcpy.Delete_management(ABH, "File")
#  Exportar a Arquivo txt
arcpy.ExportXYv_stats(bproj, "AreaBH", "COMMA", ABH, "NO_FIELD_NAMES")
# Abrir Arquivo da Area da Bacia Hidraulica
arqv=open('C:\UFC\UFC11\Shape\ABH.txt','r')
# Ler fila por fila
lerfil= arqv.readlines()
# Fechar Arquivo
arqv.close()
# Iniciar Vetor dos Pontos Iniciais
vareabh = []
# Carregar Vetor Pontos Inicias
for vetor in lerfil:
    # Cortar em Virgula
    line = vetor.split(',')
    # Carregar Ponto Inicial
    vareabh += [line[2]]

# Numero de Celulas
ambah = int((float(vareabh[0]) * 1000000 / (900)) * 0.0005)
if ambah > 4:
	# Indicar o valor minimo do FA
    acum = ambah
else:
	# Indicar o valor minimo do FA
    acum = 4

# Apagar arquivos temporarios
arcpy.Delete_management(ABH, "File")

# Variaveis Locais
RC    = "C:\UFC\UFC11\Raster\mcal.tif"
SL    = "C:\UFC\UFC11\Raster\slink.tif"
SO    = "C:\UFC\UFC11\Raster\sorder.tif"
Rios  = "C:\UFC\UFC11\Shape\Rios2Dt.shp"
RBH   = "C:\UFC\UFC11\Shape\RBH.shp"
VRBH  = "C:\UFC\UFC11\Shape\VRB.shp"
Shape = "C:\UFC\UFC11\Shape"
rtemp = "C:\UFC\UFC11\Shape\prtemp.shp"

# Apagar arquivos anteriores
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(RBH, "ShapeFile")
arcpy.Delete_management(VRBH, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")

# Clasificar os Rios pela quantidade de area drenada
arcpy.AddMessage("Classificando os Rios pela Area Drenada")
# Importar a Livraria Spatial Analyst
from arcpy.sa import *
# Cortar Raster com Bacia Hidrografica
arcpy.gp.ExtractByMask_sa(FA, Bacia, FAR)
# Condicao do Raster
outRaster = Raster(FAR) > acum
# Salvar o Raster
outRaster.save(RC)
# Unir os Pontos continuos dos Rios
arcpy.gp.StreamLink_sa(RC, FD, SL)
# Definir a Ordem dos Rios
arcpy.gp.StreamOrder_sa(SL, FD, SO, "STRAHLER")
# Exportar os rios a Shapefile
arcpy.gp.StreamToFeature_sa(SO, FD, Rios, "SIMPLIFY")
# Criar Shapefile com Propiedades Ativas de Cotas
arcpy.CreateFeatureclass_management(Shape, "prtemp.shp", "POLYLINE", "", "ENABLED", "ENABLED", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119522E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")
# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\\UFC\\UFC11\\Shape\\prtemp.shp;C:\\UFC\\UFC11\\Shape\\Rios2Dt.shp", RBH, "")
# Exportar os Vertices a Pontos
arcpy.FeatureVerticesToPoints_management(RBH, VRBH, "ALL")
# Apagar Arquivos Temporais
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")
arcpy.Delete_management(FAR, "RasterDataset")
# Verificar as licencias
arcpy.CheckInExtension("Spatial")
# Determinar as distancias dos pontos ate o Exutorio
arcpy.AddMessage("Calculo das distancias dos pontos ate o Exutorio")
# Verificar as licensas necessarias
arcpy.CheckOutExtension("spatial")
arcpy.CheckOutExtension("3D")
# Variaveis Locais
fdpr = "C:\UFC\UFC11\Raster\pfdpr.tif"
rbac = "C:\UFC\UFC11\Raster\prbac.tif"
frbac = "C:\UFC\UFC11\Raster\pfillrbac.tif"
fdbac = "C:\UFC\UFC11\Raster\pfdbac.tif"
longit = "C:\UFC\UFC11\Raster\longit.tif"
ptopro = "C:\\UFC\\UFC11\\Shape\\ptoproj.shp"
# Apagar arquivos anteriores
arcpy.Delete_management(fdpr, "RasterDataset")
arcpy.Delete_management(rbac, "RasterDataset")
arcpy.Delete_management(frbac, "RasterDataset")
arcpy.Delete_management(fdbac, "RasterDataset")
arcpy.Delete_management(longit, "RasterDataset")
# Cortar Raster com Bacia Hidrografica
arcpy.gp.ExtractByMask_sa(REXT, Bacia, rbac)
# Prencheer o Raster prbac
arcpy.gp.Fill_sa(rbac, frbac, "")
# Calcular o Flow Direction
arcpy.gp.FlowDirection_sa(frbac, fdbac, "NORMAL", "")
# Projetar Raster Flow Direction
arcpy.ProjectRaster_management(fdbac, fdpr, Projecao, "NEAREST", "30.7884903447521 30.7884903447521", "", "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Calcular Distancia dos Pontos ate Exutorio
arcpy.gp.FlowLength_sa(fdpr, longit, "DOWNSTREAM", "")
# Apagar arquivos temporais
arcpy.Delete_management(rbac, "RasterDataset")
arcpy.Delete_management(frbac, "RasterDataset")
arcpy.Delete_management(fdbac, "RasterDataset")
# Projetar Rios 2D da Bacia
arcpy.Project_management(RBH, r2dtutm, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Exportar os Vertices Inicias
arcpy.FeatureVerticesToPoints_management(r2dtutm, pvert, "START")
# Selecao dos Pontos Iniciais
arcpy.Select_analysis(pvert, PtosT, "\"GRID_CODE\" =1")
# Ordene os pontos
arcpy.Sort_management(PtosT, ptosor, "FROM_NODE ASCENDING", "UR")
# Apagar Colunas
arcpy.DeleteField_management(ptosor, "ARCID;GRID_CODE;TO_NODE;ORIG_FID")
# Adicionar o valor da distancia ate o Exutorio
arcpy.gp.ExtractMultiValuesToPoints_sa(ptosor, "C:\\UFC\\UFC11\\Raster\\longit.tif", "NONE")
# Ordem dos Pontos
# Local variables:
ptsor2 = "C:\\UFC\\UFC11\\Shape\\ptsor2.shp"
# Ordenar os pontos menor a maior
arcpy.Sort_management(ptosor, ptsor2, "longit DESCENDING", "UR")
# Apagar arquivo
arcpy.Delete_management(ptosor, "ShapeFile")
# Renomear Arquivo
arcpy.Rename_management(ptsor2, ptosor, "ShapeFile")
# Criar Folga da Bacia
arcpy.Buffer_analysis(Bacia, BTopotemp, "2 Kilometers", "FULL", "ROUND", "NONE", "")
# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\UFC\UFC11\Shape\politemp.shp;C:\UFC\UFC11\Shape\BTopotemp.shp", BTopo, "ID \"ID\" true true false 0 Long 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\BTopotemp.shp,ID,-1,-1;GRIDCODE \"GRIDCODE\" true true false 0 Long 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\BTopotemp.shp,GRIDCODE,-1,-1;BUFF_DIST \"BUFF_DIST\" true true false 0 Double 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\BTopotemp.shp,BUFF_DIST,-1,-1;ORIG_FID \"ORIG_FID\" true true false 0 Long 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\BTopotemp.shp,ORIG_FID,-1,-1")
# Extrair o Raster com a Bacia com Folga
arcpy.gp.ExtractByMask_sa(REXT, BTopo, rtop)
# Encher os Buracos do DEM
arcpy.gp.Fill_sa(rtop, rtopo, "")
# Fazer as Curvas de Nivel da Bacia
arcpy.AddMessage("Determinar as Curvas de Nivel")
arcpy.Contour_3d(rtopo, cniveltemp, Inter, "0", "1")
# Criar Shapefile com Propiedades Ativas de Cotas
arcpy.CreateFeatureclass_management(Shape, "linhawgs84.shp", "POLYLINE", "", "ENABLED", "ENABLED", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119522E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")
# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\UFC\UFC11\Shape\linhawgs84.shp;C:\UFC\UFC11\Shape\cniveltemp.shp", cnivel, "CONTOUR \"CONTOUR\" true true false 0 Double 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\cniveltemp.shp,CONTOUR,-1,-1")
# Apagar Arquivos Temporarios
arcpy.Delete_management(BH, "RasterDataset")
arcpy.Delete_management(bhtemp, "ShapeFile")
arcpy.Delete_management(politemp, "ShapeFile")
arcpy.Delete_management(BTopotemp, "ShapeFile")
arcpy.Delete_management(pvert, "ShapeFile")
arcpy.Delete_management(rtop, "RasterDataset")
arcpy.Delete_management(cniveltemp, "ShapeFile")
arcpy.Delete_management(linhawgs84, "ShapeFile")
arcpy.Delete_management(ptopro, "ShapeFile")
# Determinar as Estacoes Pluviometricas
arcpy.AddMessage("Determinar os Postos Pluviometricos")
# Maxima Distancia dos Postos ate a Bacia
Distancia = "500 Kilometers"
# Criar Folga da Bacia para os Pluviometros
arcpy.Buffer_analysis(Bacia, btopobuf, Distancia, "FULL", "ROUND", "NONE", "")
# Condicao da Selecao
condan = "\"Anos\" >=" + str(Anos)
# Selecao dos Postos con numero de anos maior
arcpy.Select_analysis(EPluvioi, EPluvio, condan)
# Cortar os Postos Pluviometricos dentro da Folga
arcpy.Clip_analysis(EPluvio, btopobuf, PPluv, "")
# Criar os Poligonos de Thiessen
arcpy.AddMessage("Criar os Poligonos de Thiessen")
arcpy.CreateThiessenPolygons_analysis(PPluv, thtem, "ALL")
# Uniao Espacial das Propriedades da Bacia e os Poligonos de Thiessen
arcpy.SpatialJoin_analysis(thtem, Bacia, thtemp, "JOIN_ONE_TO_ONE", "KEEP_ALL", "Id \"Id\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,Id,-1,-1;Input_FID \"Input_FID\" true true false 9 Long 0 9 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,Input_FID,-1,-1;Codigo \"Codigo\" true true false 9 Long 0 9 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,Codigo,-1,-1;Nome \"Nome\" true true false 50 Text 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,Nome,-1,-1;PeriodoPlu \"PeriodoPlu\" true true false 8 Date 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,PeriodoPlu,-1,-1;UltimaAtua \"UltimaAtua\" true true false 8 Date 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,UltimaAtua,-1,-1;Anos \"Anos\" true true false 5 Long 0 5 ,First,#,C:\\UFC\\UFC11\\Shape\\pthtem.shp,Anos,-1,-1;Id_1 \"Id_1\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\pbhidro.shp,Id,-1,-1;GRIDCODE \"GRIDCODE\" true true false 10 Double 0 10 ,First,#,C:\\UFC\\UFC11\\Shape\\pbhidro.shp,GRIDCODE,-1,-1;BACIA \"BACIA\" true true false 3 Short 0 3 ,First,#,C:\\UFC\\UFC11\\Shape\\pbhidro.shp,BACIA,-1,-1", "INTERSECT", "0 Meters", "")
# Selecao dos Poligonos
arcpy.Select_analysis(thtemp, ttemp, "\"BACIA\" >0")
# Apagar Colunas
arcpy.DeleteField_management(ttemp, "Join_Count;TARGET_FID;Codigo;Nome;PeriodoPlu;UltimaAtua;Anos;Input_FID;ID;GRIDCODE;Shape_length;Shape_area")
# Uniao Espacial das Propriedades dos Poligonos de Thiessen e os Postos Pluviometricos
arcpy.SpatialJoin_analysis(EPluvio, ttemp, ptem, "JOIN_ONE_TO_ONE", "KEEP_ALL", "Codigo \"Codigo\" true true false 9 Long 0 9 ,First,#,C:\\UFC\\UFC11\\Dados\\EPT.shp,Codigo,-1,-1;Nome \"Nome\" true true false 50 Text 0 0 ,First,#,C:\\UFC\\UFC11\\Dados\\EPT.shp,Nome,-1,-1;PeriodoPlu \"PeriodoPlu\" true true false 8 Date 0 0 ,First,#,C:\\UFC\\UFC11\\Dados\\EPT.shp,PeriodoPlu,-1,-1;UltimaAtua \"UltimaAtua\" true true false 8 Date 0 0 ,First,#,C:\\UFC\\UFC11\\Dados\\EPT.shp,UltimaAtua,-1,-1;Anos \"Anos\" true true false 5 Long 0 5 ,First,#,C:\\UFC\\UFC11\\Dados\\EPT.shp,Anos,-1,-1;Id_1 \"Id_1\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\pttemp.shp,Id_1,-1,-1;BACIA \"BACIA\" true true false 3 Short 0 3 ,First,#,C:\\UFC\\UFC11\\Shape\\pttemp.shp,BACIA,-1,-1", "INTERSECT", "0 Meters", "")
# Selecao dos Postos Pluviometricos contidos nos Poligonos de Thiessen da Bacia
arcpy.Select_analysis(ptem, ptemp, "\"BACIA\" >0")
# Projetar Postos Pluviometricos
arcpy.Project_management(ptemp, PPTOPO, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Adicionar Coluna Layer em Postos Pluviometricos e Calcular Valor
arcpy.AddField_management(PPTOPO, "Layer", "TEXT", "", "", "300", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(PPTOPO, "Layer", "[Nome] & \" Codigo \" & [Codigo] &\" T \"& [Anos]&\" Anos\"", "VB", "")
# Cortar os Poligonos de Thiessen com a Bacia
arcpy.Clip_analysis(thtem, Bacia, thies, "0 Meters")
# Adicionar e Calcular Coluna Postos
arcpy.AddField_management(thies, "POSTOS", "SHORT", "5", "", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(thies, "POSTOS", "1", "VB", "")
# Apagar Colunas dos Poligonos Thiessen
arcpy.DeleteField_management(thies, "PeriodoPlu;UltimaAtua;Input_FID")
# Projetar Poligonos de Thiessen
arcpy.Project_management(thies, TTOPOUTM, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Adicionar Coluna Area no Poligonos de Thiessen
arcpy.AddField_management(TTOPOUTM, "AreaPT", "DOUBLE", "15", "2", "", "", "NULLABLE", "NON_REQUIRED", "")
# Adicionar Propriedades da Geometria dos Poligonos de Thiessen
arcpy.AddGeometryAttributes_management(TTOPOUTM, "AREA", "", "SQUARE_KILOMETERS", Projecao)
# Calcular Area dos Poligonos de Thiessen
arcpy.CalculateField_management(TTOPOUTM, "AreaPT", "[POLY_AREA]", "VB", "")
# Uniao Espacial das Propriedades dos Poligonos de Thiessen e a Bacia Hidrologica
arcpy.SpatialJoin_analysis(TTOPOUTM, bproj, ThiessenTPUTM, "JOIN_ONE_TO_ONE", "KEEP_ALL", "Id \"Id\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,Id,-1,-1;Codigo \"Codigo\" true true false 9 Long 0 9 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,Codigo,-1,-1;Nome \"Nome\" true true false 50 Text 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,Nome,-1,-1;Anos \"Anos\" true true false 5 Long 0 5 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,Anos,-1,-1;POSTOS \"POSTOS\" true true false 5 Long 0 5 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,POSTOS,-1,-1;AreaPT \"AreaPT\" true true false 16 Double 2 15 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,AreaPT,-1,-1;POLY_AREA \"POLY_AREA\" true true false 19 Double 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\ThiessenTOPOUTM.shp,POLY_AREA,-1,-1;Id_1 \"Id_1\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\pbproj.shp,Id,-1,-1;BACIA \"BACIA\" true true false 3 Short 0 3 ,First,#,C:\\UFC\\UFC11\\Shape\\pbproj.shp,BACIA,-1,-1;AreaBH \"AreaBH\" true true false 16 Double 2 15 ,First,#,C:\\UFC\\UFC11\\Shape\\pbproj.shp,AreaBH,-1,-1", "INTERSECT", "0 Meters", "")
# Uniao Espacial das Propriedades dos Pontos Iniciais e a Bacia Hidrologica
arcpy.SpatialJoin_analysis(ptosor, bproj, ptopro, "JOIN_ONE_TO_ONE", "KEEP_ALL", "Id \"Id\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\ptsor.shp,Id,-1,-1;FROM_NODE \"FROM_NODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\ptsor.shp,FROM_NODE,-1,-1;longit \"longit\" true true false 13 Float 0 0 ,First,#,C:\\UFC\\UFC11\\Shape\\ptsor.shp,longit,-1,-1;PerimBH \"PerimBH\" true true false 19 Double 0 19 ,First,#,C:\\UFC\\UFC11\\Shape\\pbproj.shp,PerimBH,-1,-1", "INTERSECT", "0 Meters", "")
# Variaveis Locais
pini = "C:\\UFC\\UFC11\\Shape\\pini.shp"
ptoini = "C:\\UFC\\UFC11\\Shape\\ptoini.txt"
# Apagar Arquivos Anteriores
arcpy.Delete_management(pini, "ShapeFile")
arcpy.Delete_management(ptoini, "File")
# Renomear Arquivo
arcpy.Rename_management(ptopro, pini, "ShapeFile")
# Exportar a Arquivo txt
arcpy.ExportXYv_stats(pini, "FROM_NODE", "COMMA", ptoini, "NO_FIELD_NAMES")
# Apagar Colunas dos Poligonos de Thiessen
arcpy.DeleteField_management(ThiessenTPUTM, "Join_Count;TARGET_FID;POSTOS;POLY_AREA;ID")
# Adicionar e Calcular Coluna Percent
arcpy.AddField_management(ThiessenTPUTM, "Percent", "DOUBLE", "5", "2", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(ThiessenTPUTM, "Percent", "[AreaPT]/ [AreaBH]*100", "VB", "")
# Adicionar e Calcular Coluna Ativo
arcpy.AddField_management(ThiessenTPUTM, "Ativo", "SHORT", "4", "", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(ThiessenTPUTM, "Ativo", "1", "VB", "")
# Exportar Tabelas para Excel
arcpy.TableToExcel_conversion(ThiessenTPUTM, Estacoes, "NAME", "CODE")
# Apagar Arquivos Temporarios
arcpy.Delete_management(btopobuf, "ShapeFile")
arcpy.Delete_management(PPluv, "ShapeFile")
arcpy.Delete_management(thtem, "ShapeFile")
arcpy.Delete_management(thtemp, "ShapeFile")
arcpy.Delete_management(ttemp, "ShapeFile")
arcpy.Delete_management(ptem, "ShapeFile")
arcpy.Delete_management(ptemp, "ShapeFile")
arcpy.Delete_management(thies, "ShapeFile")
arcpy.Delete_management(TTOPOUTM, "ShapeFile")
arcpy.Delete_management(EPluvio, "ShapeFile")
arcpy.Delete_management(bproj, "ShapeFile")
arcpy.Delete_management(ptosor, "ShapeFile")
# Projetar Arquivos
arcpy.AddMessage("Projetar Arquivos")
# Verificar as licencias necesarias
arcpy.CheckOutExtension("3D")
# Variaveis Resultados
CNUTM = "C:\UFC\UFC11\Saida\CNUTM.shp"
vtopoutm = "C:\UFC\UFC11\Saida\Vtopoutm.shp"
btopoutm = "C:\UFC\UFC11\Saida\Btopoutm.shp"
Rastopoutm = "C:\UFC\UFC11\Raster\Rastopoutm.tif"
Bacia3D = "C:\UFC\UFC11\Saida\Bacia3DTopo.shp"
RPtos = "C:\\UFC\\UFC11\\Shape\\RPtos.txt"
# Variavel Temporal
rsor = "C:\\UFC\\UFC11\\Shape\\rsort.shp"
# Apagar arquivos anteriores
arcpy.Delete_management(CNUTM, "ShapeFile")
arcpy.Delete_management(btopoutm, "ShapeFile")
arcpy.Delete_management(vtopoutm, "ShapeFile")
arcpy.Delete_management(Rastopoutm, "RasterDataset")
arcpy.Delete_management(Bacia3D, "ShapeFile")
arcpy.Delete_management(rsor, "ShapeFile")
arcpy.Delete_management(RPtos, "File")
# Adicionar e Calcular Coluna Elevation no Shape Curvas de Nivel
arcpy.AddField_management(cnivel, "Elevation", "DOUBLE", "12", "4", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(cnivel, "Elevation", "[CONTOUR]", "VB", "")
# Projetar Arquivos Shape
arcpy.Project_management(cnivel, CNUTM, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
arcpy.Project_management(VRBH, vtopoutm, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
arcpy.Project_management(Bacia, btopoutm, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Projetar Raster
arcpy.ProjectRaster_management(rtopo, Rastopoutm, Projecao, "NEAREST", "30.7861210868436 30.7861210868436", "", "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Interpolar Arquivo Shape
arcpy.InterpolateShape_3d(Rastopoutm, btopoutm, Bacia3D, "", "1", "BILINEAR", "DENSIFY", "0")
# Ordear coluna FROM NODE
arcpy.Sort_management(r2dtutm, rsor, "FROM_NODE ASCENDING", "UR")
# Adicionar Propriedades Geometricas dos Rios
arcpy.AddGeometryAttributes_management(rsor, "LENGTH", "METERS", "SQUARE_METERS", Projecao)
# Adicionar e Calcular Coluna long
arcpy.AddField_management(rsor, "long", "DOUBLE", "100", "2", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(rsor, "long", "[LENGTH]", "VB", "")
# Adicionar e Calcular Coluna pral
arcpy.AddField_management(rsor, "Pral", "SHORT", "5", "", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.CalculateField_management(rsor, "Pral", "0", "VB", "")
# Exportar a txt os trechos dos rios
arcpy.ExportXYv_stats(rsor, "FROM_NODE;TO_NODE;long", "COMMA", RPtos, "NO_FIELD_NAMES")
# Apagar Arquivos Temporarios
arcpy.Delete_management(rsor, "ShapeFile")
# Calculo do Canal Principal
arcpy.AddMessage("Calculo do Canal Principal ===")
# Abrir Arquivo de Texto dos Pontos Inicias
arquivo=open('C:\UFC\UFC11\Shape\ptoini.txt','r')
# Ler fila por fila
lerfila= arquivo.readlines()
# Fechar Arquivo
arquivo.close()
# Iniciar Vetor dos Pontos Iniciais
ptoinicial0 = []
# Carregar Vetor Pontos Inicias
for vetor in lerfila:
    # Cortar em Virgula
    linea = vetor.split(',')
    # Carregar Ponto Inicial
    ptoinicial0 += [linea[2]]
# Numero de Pontos
nptin = len(ptoinicial0)
# Selecionar os Pontos Maiores
if nptin <= 50:
    # Se o numero de pontos e menor a 50
    mnp = nptin
else:
    # Logaritmo do Numero Total de Pontos
    mnpto = int(math.log10(nptin)*50)
    if mnpto <= 50:
        # Garantir um numero minimo de 50
        mnp = 50
    else:
        # O numero de Pontos e o numero calculado
        mnp = mnpto
# Carregar Vetor Pontos Inicias
npt0 = 0
# Iniciar Vetor dos Pontos Iniciais
ptoinicial = []
while npt0 < mnp:
    # Carregar Ponto Inicial
    ptoinicial += [ptoinicial0[npt0]]
    npt0 = npt0 + 1
# Abrir Arquivo de Texto dos Rios
arqrios=open('C:\UFC\UFC11\Shape\RPtos.txt','r')
# Ler fila por fila
lerfilas= arqrios.readlines()
# Fechar Arquivo
arqrios.close()
# Iniciar Vetores dos Rios
rioini = []
riofim = []
longrio = []
# Carregar Vetores
for vetores in lerfilas:
    # Cortar em Virgula
    lineas = vetores.split(',')
    # Carregar Vetores Rios
    rioini += [lineas[2]]
    riofim += [lineas[3]]
    longrio += [lineas[4]]
# Procurar Ponto mais longe
# Calcular Tamanho Vetor Rios
nrio = len(rioini)
# Iniciar Funcao Suma dos Comprimentos
# Definir Funcao
def compto (pto):
    # Iniciar Contadores Comprimento e numero
    lonr = 0
    nr = 0
    # Comprimento do Ponto
    # Iterar para cada um dos Pontos Inicias
    while nr <= nrio-1:
        # Comprimento Trecho
        lpar = float(longrio[nr])
        # Comparar se o Ponto e Igual ao Ponto Inicial do Rio
        if pto == str(rioini[nr]):
            # Sumar os Comprimentos
            lonr = lonr + lpar
            # Igualar o Ponto com o Ponto Final
            pto = str(riofim[nr])
            # Reiniciar Contador
            nr = 0
        # Se nao adicionar o contador em +1
        else:
            # Aumentar o Contador em +1
            nr = nr + 1
    # Retornar o Valor Total do Comprimento
    return lonr
# Iniciar Contador dos Pontos Iniciais
npt = 0
# Calcular Tamanho Vetor Pontos Inicias
lpto = len(ptoinicial)
# Iniciar Comprimento do Canal Principal
lcomp = float(0)
# Iniciar a Posicao de Comparacao
pvpto = int(0)
# Iniciar Lazo para todos os Pontos Iniciais
while npt < lpto:
    # Valores de Ponto Inicial
    picom = int(ptoinicial[npt])
    # Comprimento do Ponto Inicial
    locom = compto(str(picom))
    # Comparar os Comprimentos
    if locom > lcomp:
        # Igualar o Comprimento de Comparacao
        lcomp = locom
        # Capturar a Posicao do Ponto
        pvpto = npt
    # Aumentar o Contador em +1
    npt = npt + 1
# Criar Vetor dos Trechos
# Iniciar Vetor Vacio
vtal = []
# Iniciar Contador dos Pontos
nrt = 0
# Iniciar Valor de Comparacao
ptos = int(ptoinicial[pvpto])
arcpy.AddMessage(ptos)
# Iterar para cada um dos Pontos Inicias
arcpy.AddMessage("Comparando os Pontos Iniciais")
while nrt <= nrio-1:
    vcom = int(rioini[nrt])
    # Comparar se o Ponto e Igual ao Ponto Inicial do Rio
    if ptos == vcom:
        # Adicionar o Ponto ao Vetor vtal
        vtal += [str(ptos)]
        # Igualar o Ponto com o Ponto Final
        ptos = int(riofim[nrt])
        # Reiniciar Contador
        nrt = 0
    # Se nao adicionar o contador em +1
    else:
        # Aumentar o Contador em +1
        nrt = nrt + 1
# Variaveis Locais
talv = "C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp"
talv1 = "C:\\UFC\\UFC11\\Shape\\talv1.shp"
talv2 = "C:\\UFC\\UFC11\\Shape\\talv2.shp"
# Apagar Arquivos anteriores
arcpy.Delete_management(talv, "ShapeFile")
arcpy.Delete_management(talv1, "ShapeFile")
arcpy.Delete_management(talv2, "ShapeFile")
# Numero de Rios
nrot = len(vtal)
# Condicao da Selecao
cond = "\"FROM_NODE\" =" + str(vtal[0])
# Selecao dos Pontos
arcpy.Select_analysis(RBH, talv, cond)
# Inicio do Lazo FOR para Carregado do Vetor Principal
for nro in range(1, nrot):
    # Condicao da Selecao
    cond = "\"FROM_NODE\" =" + str(vtal[nro])
    # Selecao dos Pontos
    arcpy.Select_analysis(RBH, talv1, cond)
    # Uniao dos Rios
    arcpy.Merge_management("C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp;C:\\UFC\\UFC11\\Shape\\talv1.shp", talv2, "Id \"Id\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp,Id,-1,-1,C:\\UFC\\UFC11\\Shape\\talv1.shp,Id,-1,-1;ARCID \"ARCID\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp,ARCID,-1,-1,C:\\UFC\\UFC11\\Shape\\talv1.shp,ARCID,-1,-1;GRID_CODE \"GRID_CODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp,GRID_CODE,-1,-1,C:\\UFC\\UFC11\\Shape\\talv1.shp,GRID_CODE,-1,-1;FROM_NODE \"FROM_NODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp,FROM_NODE,-1,-1,C:\\UFC\\UFC11\\Shape\\talv1.shp,FROM_NODE,-1,-1;TO_NODE \"TO_NODE\" true true false 6 Long 0 6 ,First,#,C:\\UFC\\UFC11\\Shape\\CanalPralTD.shp,TO_NODE,-1,-1,C:\\UFC\\UFC11\\Shape\\talv1.shp,TO_NODE,-1,-1")
    # Apagar arquivos
    arcpy.Delete_management(talv, "ShapeFile")
    arcpy.Delete_management(talv1, "ShapeFile")
    # Renomear Shapefile
    arcpy.Rename_management(talv2, talv, "")
# Disolver o Canal Principal
talvTDtemp = "C:\\UFC\\UFC11\\Shape\\talvTD.shp"
# Apagar arquivos anteriores
arcpy.Delete_management(talvTDtemp, "ShapeFile")
# Disolver o Canal Principal
arcpy.Dissolve_management(talv, talvTDtemp, "Id", "", "MULTI_PART", "DISSOLVE_LINES")
# Apagar arquivos
arcpy.Delete_management(talv, "ShapeFile")
# Renomear Shapefile
arcpy.Rename_management(talvTDtemp, talv, "ShapeFile")
# Variavel Resultado
ThPro = "C:\\UFC\\UFC11\\Saida\\CanalPral.shp"
Talv3D = "C:\\UFC\\UFC11\\Saida\\CanalPral3DTOPO.shp"
# Apagar Arquivos Anteriores
arcpy.Delete_management(ThPro, "ShapeFile")
arcpy.Delete_management(Talv3D, "ShapeFile")
# Projetar Arquivos Canal Principal
arcpy.Project_management(talv, ThPro, Projecao, "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Interpolar Arquivo Canal Principal
arcpy.InterpolateShape_3d(Rastopoutm, ThPro, Talv3D, "", "1", "BILINEAR", "DENSIFY", "0")
# Apagar Arquivos Temporarios
arcpy.AddMessage("Apagar Arquivos Temporarios")
arcpy.Delete_management(RPtos, "File")
arcpy.Delete_management(ptoini, "File")
arcpy.Delete_management(ptosor, "ShapeFile")
arcpy.Delete_management(PtosT, "ShapeFile")
arcpy.Delete_management(pini, "ShapeFile")
# Exportar Retangulo Alos Palsar
arcpy.AddMessage("Exportar Retangulo Alos Palsar")
# Variaveis Locais
ssbuf = "C:\UFC\UFC11\Shape\ssbuf.shp"
VB = "C:\UFC\UFC11\Shape\VB.shp"
APTXT = "C:\UFC\UFC11\ALOSPALSAR.txt"
dtab = "C:\UFC\UFC11\Shape\drap.dbf"
Tabela = "C:\UFC\UFC11\Shape\Resultados.xls"
# Apagar Arquivos anteriores
arcpy.Delete_management(ssbuf, "ShapeFile")
arcpy.Delete_management(VB, "ShapeFile")
arcpy.Delete_management(dtab, "DbaseTable")
arcpy.Delete_management(Tabela, "File")
arcpy.Delete_management(APTXT, "File")
# Criar Folga
arcpy.Buffer_analysis(Bacia, ssbuf, "150 Meters", "FULL", "ROUND", "NONE", "")
# Exportar Vertices para Pontos
arcpy.FeatureVerticesToPoints_management(ssbuf, VB, "ALL")
# Adicionar Colunas Longitude e Latitude
arcpy.AddField_management(VB, "Longitude", "DOUBLE", "10", "2", "", "", "NULLABLE", "NON_REQUIRED", "")
arcpy.AddField_management(VB, "Latitude", "DOUBLE", "10", "2", "", "", "NULLABLE", "NON_REQUIRED", "")
# Adicionar Propriedades Geometricas
arcpy.AddGeometryAttributes_management(VB, "POINT_X_Y_Z_M", "", "", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]]")
# Calcular Colunas
arcpy.CalculateField_management(VB, "Longitude", "[POINT_X]", "VB", "")
arcpy.CalculateField_management(VB, "Latitude", "[POINT_Y]", "VB", "")
# Apagar Colunas
arcpy.DeleteField_management(VB, "GRIDCODE;BUFF_DIST;ORIG_FID;POINT_X;POINT_Y;POINT_Z;POINT_M")
# Calcular valores Maximos e Minimos
arcpy.Statistics_analysis(VB, dtab, "Longitude MAX;Longitude MIN;Latitude MAX;Latitude MIN", "")
# Exportar para Tabela Excel
arcpy.TableToExcel_conversion(dtab, Tabela, "NAME", "CODE")
# Ler Arquivo Excel Resultado.xls
excel = xlrd.open_workbook("C:\UFC\UFC11\Shape\Resultados.xls")
# Ler arquivo Resultados.xls
lvr = excel.sheet_by_name("Resultados")
# Ler a primeira planilha
MINLONG = lvr.cell_value(1,3)
## Minima Longitude
MAXLONG = lvr.cell_value(1,2)
## Maxima Longitude
MINLAT =  lvr.cell_value(1,5)
## MIN Latitude
MAXLAT =  lvr.cell_value(1,4)
## Maxima Latitude
def creartxt():
    archi=open('C:\UFC\UFC11\ALOSPALSAR.txt','w')
    archi.close()
def salvartxt():
    archi=open('C:\UFC\UFC11\ALOSPALSAR.txt','a')
    archi.write(str(MINLONG)+','+str(MINLAT)+','+str(MAXLONG)+','+str(MINLAT)+','+str(MAXLONG)+','+str(MAXLAT)+','+str(MINLONG)+','+str(MAXLAT)+','+str(MINLONG)+','+str(MINLAT))
    archi.close()
creartxt()
salvartxt()
# Apagar Arquivos temporarios
arcpy.Delete_management(ssbuf, "ShapeFile")
arcpy.Delete_management(VB, "ShapeFile")
arcpy.Delete_management(dtab, "DbaseTable")
arcpy.Delete_management(Tabela, "File")

# Exportar os arquivos para CAD
arcpy.AddMessage("Exportando os arquivos para CAD")
BTOPO = "C:\\UFC\\UFC11\\Saida\\BTOPO.DWG"
CNivelT = "C:\\UFC\\UFC11\\Saida\\CNivelTOPOUTM.DWG"
RTOPO = "C:\\UFC\\UFC11\\Saida\\RTOP.dwg"
PPTopo = "C:\\UFC\\UFC11\\Saida\\PPluvioTopo.DWG"
TTOPO = "C:\\UFC\\UFC11\\Saida\\CanalPralTOPO.dwg"
ThTopo = "C:\\UFC\\UFC11\\Saida\\ThiessenTopo.dwg"
B3DTPO = "C:\\UFC\\UFC11\\Saida\\B3DTOPO.DWG"
T3DTOPO = "C:\\UFC\\UFC11\\Saida\\CanalPral3DTOPO.dwg"

# Apagar Arquivos anteriores
arcpy.Delete_management(BTOPO, "CadDrawingDataset")
arcpy.Delete_management(CNivelT, "CadDrawingDataset")
arcpy.Delete_management(RTOPO, "CadDrawingDataset")
arcpy.Delete_management(PPTopo, "CadDrawingDataset")
arcpy.Delete_management(TTOPO, "CadDrawingDataset")
arcpy.Delete_management(ThTopo, "CadDrawingDataset")
arcpy.Delete_management(B3DTPO, "CadDrawingDataset")
arcpy.Delete_management(T3DTOPO, "CadDrawingDataset")

# Exportar para CAD (AutoCAD)
#arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\btopoutm.shp", "DWG_R2010", BTOPO, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\CNUTM.shp", "DWG_R2010", CNivelT, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Shape\\Rtputm.shp", "DWG_R2010", RTOPO, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\PPluvioTOPO.shp", "DWG_R2010", PPTopo, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\ThiessenTPUTM.shp", "DWG_R2010", ThTopo, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\Bacia3DTopo.shp", "DWG_R2010", B3DTPO, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
arcpy.ExportCAD_conversion("C:\\UFC\\UFC11\\Saida\\CanalPral3DTOPO.shp", "DWG_R2010", T3DTOPO, "Ignore_Filenames_in_Tables", "Overwrite_Existing_Files", "")
copy2('C:\UFC\UFC11\Shape\{0}_bkp.zip'.format(name_exutshp), r'C:\UFC\UFC11\Saida')

arcpy.AddMessage('\n + Arquivos exportados para CAD:')
arcpy.AddMessage(' + {0}'.format(CNivelT))
arcpy.AddMessage(' + {0}'.format(RTOPO))
arcpy.AddMessage(' + {0}'.format(PPTopo))
arcpy.AddMessage(' + {0}'.format(ThTopo))
arcpy.AddMessage(' + {0}'.format(B3DTPO))
arcpy.AddMessage(' + {0}'.format(T3DTOPO))
arcpy.AddMessage('\n + Outros arquivos salvos na pasta C:\\UFC\\UFC11\\Saida:')
arcpy.AddMessage(' + {0}_bkp.zip'.format(name_exutshp))

os.remove('C:\UFC\UFC11\Shape\{0}_bkp.zip'.format(name_exutshp))
os.remove(Exut)
arcpy.AddMessage('\n + Arquivos removidos:')
arcpy.AddMessage(' + C:\UFC\UFC11\Shape\{0}_bkp.zip'.format(name_exutshp))
arcpy.AddMessage(' + {0}'.format(Exut))

raise SystemExit

# Visualizar os arquivos resultados
arcpy.AddMessage("Adicionando os Arquivos Resultados na Tela Atual")
# Definir o Arquivo de ArcGIS atual.
tatual = arcpy.mapping.MapDocument("Current")
# Definir o Layer Atual
camadaatual = arcpy.mapping.ListDataFrames(tatual,"*")[0]
# Criar uma Nova Camada
camada1 = arcpy.mapping.Layer(Bacia)
camada2 = arcpy.mapping.Layer(RBH)
camada3 = arcpy.mapping.Layer(cnivel)
camada4 = arcpy.mapping.Layer(Exut)
camada5 = arcpy.mapping.Layer(PPTOPO)
camada6 = arcpy.mapping.Layer(ThiessenTPUTM)
camada7 = arcpy.mapping.Layer(talv)
# Adicionar a Nova Camada no arquivo atual
arcpy.mapping.AddLayer(camadaatual, camada7,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada6,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada5,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada4,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada3,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada2,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada1,"Bottom")
# Atualizar los Arquivos
arcpy.RefreshActiveView()
arcpy.RefreshTOC()
# Fazer Zoom na Bacia
# Escolher o Documento Atual
mxd = arcpy.mapping.MapDocument('current')
# Escolher o Layer Atual
df = arcpy.mapping.ListDataFrames(mxd)[0]
# Escolher o Shape da Bacia
lyr = arcpy.mapping.ListLayers(mxd, 'pbhidro*', df)[0]
# Definir a Variavel pra fazer Zoom
ext = lyr.getExtent()
# Fazer Zoom to Layer
df.extent = ext
