import osgeo.ogr
import osgeo.osr
import osr

from osgeo import ogr, osr, gdal
from gdalconst import GA_Update

import os
import sys
import qgis
from qgis.core import *
from qgis.core import QgsApplication
from qgis.core import QgsProject
from qgis.core import QgsProcessingFeedback
from qgis.core import QgsVectorLayer
from qgis.analysis import QgsNativeAlgorithms


def create_point(nome_shape, ponto_leste, ponto_norte):
    '''
    ref: http://bit.ly/2XtOweG

    nome_shape: str
    ponto_leste: float
    ponto_oeste: float
    '''

    # will create a spatial reference locally to tell
    # the system what the reference will be
    spatialReference = osgeo.osr.SpatialReference()

    # here we define this reference to be utm Zone X with wgs84...
    spatialReference.ImportFromProj4('+proj=utm +zone=24S +ellps=WGS84 +datum=WGS84 +units=m')
    # spatialReference.ImportFromProj4('+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs')

    # will select the driver foir our shp-file creation.
    driver = osgeo.ogr.GetDriverByName('ESRI Shapefile')

    # so there we will store our data
    shapeData = driver.CreateDataSource(nome_shape)

    # this will create a corresponding layer for our
    # data with given spatial information.
    layer = shapeData.CreateLayer('layer1', spatialReference, osgeo.ogr.wkbPoint)

    # gets parameters of the current shapefile
    layer_defn = layer.GetLayerDefn()
    point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)

    # create a new point at given ccordinates
    point.AddPoint(ponto_leste, ponto_norte)

    # lon, lat, _ = transform_utm_to_wgs84(ponto_leste, ponto_norte)

    # point.SetPoint(0, lon, lat)

    # this will be the first point in our dataset
    featureIndex = 1

    # now lets write this into our layer/shape file:
    feature = osgeo.ogr.Feature(layer_defn)

    feature.SetGeometry(point)

    feature.SetFID(featureIndex)

    layer.CreateFeature(feature)

    # lets close the shapefile
    shapeData.Destroy()


def create_point2(nome_shape):
    spatialReference = osgeo.osr.SpatialReference() #will create a spatial reference locally to tell the system what the reference will be

    spatialReference.ImportFromProj4('+proj=utm +zone=24S +ellps=WGS84 +datum=WGS84 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs') #here we define this reference to be utm Zone 48N with wgs84...

    driver = osgeo.ogr.GetDriverByName('ESRI Shapefile') # will select the driver foir our shp-file creation.

    shapeData = driver.CreateDataSource(nome_shape) #so there we will store our data

    layer = shapeData.CreateLayer('customs', spatialReference, osgeo.ogr.wkbPoint) #this will create a corresponding layer for our data with given spatial information.

    layer_defn = layer.GetLayerDefn() # gets parameters of the current shapefile

    point = osgeo.ogr.Geometry(osgeo.ogr.wkbPoint)

    point.AddPoint(555419.61, 9524044.62) #create a new point at given ccordinates
    # point.AddPoint(474595, 5429281) #create a new point at given ccordinates

    featureIndex = 0 #this will be the first point in our dataset

    ##now lets write this into our layer/shape file:
    feature = osgeo.ogr.Feature(layer_defn)

    feature.SetGeometry(point)

    feature.SetFID(featureIndex)

    layer.CreateFeature(feature)

    # ## lets add now a second point with different coordinates:
    # point.AddPoint(575130.56, 9533271.76)

    # featureIndex = 1

    # feature = osgeo.ogr.Feature(layer_defn)

    # feature.SetGeometry(point)

    # feature.SetFID(featureIndex)

    # layer.CreateFeature(feature)

    shapeData.Destroy() #lets close the shapefile

    # shapeData = ogr.Open(nome_shape, 1)

    # layer = shapeData.GetLayer() #get possible layers. was source.GetLayer

    # layer_defn = layer.GetLayerDefn()

    # field_names = [layer_defn.GetFieldDefn(i).GetName() for i in range(layer_defn.GetFieldCount())] #store the field names as a list of stringsprint len(field_names)# so there should be just one at the moment called "FID"

    # print(len(field_names))# so there should be just one at the moment called "FID"

    # field_names #will show you the current field names

    # new_field = ogr.FieldDefn('HOMETOWN', ogr.OFTString) #we will create a new field called Hometown as String

    # layer.CreateField(new_field) #self explaining

    # new_field = ogr.FieldDefn('VISITS', ogr.OFTInteger) #and a second field 'VISITS' stored as integer

    # layer.CreateField(new_field) #self explaining

    # field_names = [layer_defn.GetFieldDefn(i).GetName() for i in range(layer_defn.GetFieldCount())]

    # field_names #WOOHAA!

    # feature = layer.GetFeature(0) #lets get the first feature (FID=='0')

    # i = feature.GetFieldIndex("HOMETOWN") #so iterate along the field-names and store it in iIndex

    # feature.SetField(i, 'Chicago') #exactly at this position I would like to write 'Chicago'

    # layer.SetFeature(feature) #now make the change permanent

    # feature = layer.GetFeature(1)

    # i = feature.GetFieldIndex("HOMETOWN")

    # feature.SetField(i, 'Berlin')

    # layer.SetFeature(feature)

    # shapeData = None #lets close the shape file again.


def transform_utm_to_wgs84(ponto_leste, ponto_norte, zone=24, is_northern=False):
    '''
    ref: https://stackoverflow.com/a/10239676
    '''

    utm_coordinate_system = osr.SpatialReference()

    # Set geographic coordinate system to handle lat/lon
    utm_coordinate_system.SetWellKnownGeogCS("WGS84")

    utm_coordinate_system.SetUTM(zone, is_northern)

    # Clone ONLY the geographic coordinate system
    wgs84_coordinate_system = utm_coordinate_system.CloneGeogCS()

    # create transform component
    # (<from>, <to>)
    utm_to_wgs84_transform = osr.CoordinateTransformation(utm_coordinate_system, wgs84_coordinate_system)

    lon, lat, altitude = utm_to_wgs84_transform.TransformPoint(ponto_leste, ponto_norte, 0)

    return lon, lat, altitude


def transform_wgs84_to_utm(lon, lat):
    '''
    Ainda precisa testar!
    ref: https://stackoverflow.com/a/10239676
    '''

    def get_utm_zone(longitude):
        return (int(1 + (longitude + 180.0) / 6.0))

    def is_northern(latitude):
        """
        Determines if given latitude is a northern for UTM
        """
        if (latitude < 0.0):
            return 0
        else:
            return 1

    utm_coordinate_system = osr.SpatialReference()

    # Set geographic coordinate system to handle lat/lon
    utm_coordinate_system.SetWellKnownGeogCS("WGS84")

    utm_coordinate_system.SetUTM(get_utm_zone(lon), is_northern(lat))

    # Clone ONLY the geographic coordinate system
    wgs84_coordinate_system = utm_coordinate_system.CloneGeogCS()

    # create transform component
    # (<from>, <to>)
    wgs84_to_utm_transform = osr.CoordinateTransformation(wgs84_coordinate_system, utm_coordinate_system)

    ponto_leste, ponto_norte, altitude = wgs84_to_utm_transform.TransformPoint(lon, lat, 0)

    return ponto_leste, ponto_norte, altitude

    # prox passo: verificar o mesmo ponto no UFC


def set_epsg(tiff_file, new_epsg=4326, tiff_band=1):
    dataset = gdal.Open(tiff_file, GA_Update)
    #FIXME: não tenho certeza se a var band está sendo usada
    band = dataset.GetRasterBand(tiff_band)
    srs = osr.SpatialReference()
    srs.ImportFromEPSG(new_epsg)
    dataset.SetProjection(srs.ExportToWkt())
    dataset = None
    return


def setzfromraster(ifile_shp, dem_file, ofile_shp, count):

    print('\n +++ Qgis +++ \n')

    path_qgis_installation = r'C:/OSGeo4W64/apps/qgis'
    os.environ['path_qgis_installation'] = path_qgis_installation
    QgsApplication.setPrefixPath(os.environ['path_qgis_installation'], True)

    qgs = QgsApplication([], False)
    qgs.initQgis()

    sys.path.append(r'C:/OSGeo4W64/apps/qgis/python/plugins')

    import processing
    from processing.core.Processing import Processing

    Processing.initialize()
    QgsApplication.processingRegistry().addProvider(QgsNativeAlgorithms())

    # exts = ['.shp', '.dbf', '.prj', '.qpj', '.shx']
    # for ext in exts:
    #     curr_file = f"output_files\\{count}_temp_sub_basin_line{ext}"
    #     if os.path.exists(curr_file):
    #         os.remove(curr_file)
    #         print(f'removido: {curr_file}')

    input_file = QgsVectorLayer(ifile_shp)
    temp_output_file = f"output_files\\{count}_temp_sub_basin_line.shp"

    processing.run("qgis:polygonstolines",
        {'INPUT': input_file,
         'OUTPUT': temp_output_file}
    )

    input_file = QgsVectorLayer(temp_output_file)
    processing.run("qgis:setzfromraster",
        {'BAND': 1,
         'INPUT': input_file,
         'RASTER': dem_file,
         'NODATA': 0,
         'OUTPUT': ofile_shp,
         'SCALE': 1}
    )

    # qgs.exitQgis()

    # os.remove(temp_output_file)

    return


def utm2sirgas2000(number_zone, letter_zone):

    # ref: http://processamentodigital.com.br/2013/07/27/lista-dos-codigos-epsg-mais-utilizados-no-brasil/
    #      https://www.researchgate.net/publication/319423368_Brasil_fusos_e_zonas_UTM
    #      https://spatialreference.org/ref/epsg/

    # somente para o Brasil

    sirgascode = {
        '18n': 31972,
        '19n': 31973,
        '20n': 31974,
        '18s': 31978,
        '19s': 31979,
        '20s': 31980,
        '21s': 31981,
        '22s': 31982,
        '23s': 31983,
        '24s': 31984,
        '25s': 31985
    }

    l_zones = ['h', 'j', 'k', 'l', 'm', 'n']

    if letter_zone.lower() in l_zones:
        letter_zone = 's'

    zone = f'{number_zone}{letter_zone}'

    return sirgascode[zone]
