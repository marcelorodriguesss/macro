@echo off
call "C:\OSGeo4W64\bin\o4w_env.bat"
call "C:\OSGeo4W64\bin\qt5_env.bat"
call "C:\OSGeo4W64\bin\py3_env.bat"

@echo off
path %OSGEO4W_ROOT%\apps\qgis\bin;%OSGEO4W_ROOT%\apps\grass\grass78\lib;%PATH%
set QGIS_PREFIX_PATH=%OSGEO4W_ROOT:\=/%/apps/qgis

rem path C:\OSGeo4W64\apps\qgis\bin;C:\OSGeo4W64\apps\grass\grass78\lib;%PATH%
rem set QGIS_PREFIX_PATH=C:\OSGeo4W64:\=/%/apps/qgis

set GDAL_FILENAME_IS_UTF8=YES

rem Set VSI cache to be used as buffer, see #6448
set VSI_CACHE=TRUE
set VSI_CACHE_SIZE=1000000

set QT_PLUGIN_PATH=%OSGEO4W_ROOT%\apps\qgis\qtplugins;%OSGEO4W_ROOT%\apps\qt5\plugins
set PYTHONPATH=%OSGEO4W_ROOT%\apps\qgis\python;%PYTHONPATH%

set GISBASE=%OSGEO4W_ROOT%\apps\grass\grass78\lib
set GRASSBIN=%OSGEO4W_ROOT%\bin\grass78.bat

rem set QT_PLUGIN_PATH=C:\OSGeo4W64\apps\qgis\qtplugins;C:\OSGeo4W64\apps\qt5\plugins
rem set PYTHONPATH=C:\OSGeo4W64\apps\qgis\python;%PYTHONPATH%

rem "%PYTHONHOME%\python" %*

@echo on
@if [%1]==[] (echo run o-help for a list of available commands & cmd.exe /k) else (cmd /c "%*")
