'''
Este script salva um shape com o ponto de analise (PAnalise)
'''

import os
import zipfile
import shutil
import time

import macroQ_paths
from funcs_macroQ import create_point2
from funcs_macroQ import transform_utm_to_wgs84

start_time = time.time()

print('\n = = = = = = = = = = = LOG MACRO = = = = = = = = = =')

# UFC11 = macroQ_paths.MACRO_PATHS['MACRO_ROOTDIR']

UFC11 = os.path.dirname(os.path.abspath(__file__))

PPonto = f'{UFC11}\\output_files\\Ponto'

if os.path.isdir(PPonto):
	shutil.rmtree(PPonto, ignore_errors=True)
	print(f' + Removendo pasta .............: {PPonto} - OK!')

os.makedirs(PPonto)
print(f' + Criando pasta ...............: {PPonto} - OK!')

PAnalise     = f'{UFC11}\\Dados\\PAnalise.shp'
PAnalise_bkp = f'{UFC11}\\Dados\\PAnalise_bkp.zip'

# Apagar arquivos e pasta de rodadas anteriores
list_files = [PAnalise, PAnalise_bkp]
for files in list_files:
    try:
        os.remove(files)
        print(f' + Removendo Arquivo ...........: {files} - OK!')
    except OSError:
        pass

# ponto_leste = 555419.61
# ponto_norte = 9524044.62
# create_point(PAnalise, ponto_leste, ponto_norte)
create_point2(PAnalise)

# a = transform_utm_to_wgs84(555419.61, 9524044.62)

# print(a)
