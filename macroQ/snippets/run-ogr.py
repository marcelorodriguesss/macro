import os
import ogr2ogr
import utm

import funcs_macroQ

directory = os.path.dirname(os.path.abspath(__file__))

os.chdir(f"{directory}\\output_files")

# print(os.getcwd())

input_shp = "sub_basin_3d.shp"  # EPSG:4326
reproj_shp = "sub_basin_3d_reproj.shp"

# https://github.com/Turbo87/utm
# https://pypi.org/project/utm/
# get lat, lon from input_shape
easting, nothing, z_number, z_letter = utm.from_latlon(-4.72347, -39.55143)

print(easting, nothing, z_number, z_letter)

new_epsg = funcs_macroQ.utm2sirgas2000(z_number, z_letter)

# muda sistema de coordenadas
ogr2ogr.main(["", "-s_srs", "EPSG:4326",
              "-t_srs", f"EPSG:{new_epsg}"
              "-f", reproj_shp,
              input_shp])

print(' == convertendo sistema de coordenadas para utm == ')

output_dxf = "sub_basin_3d.dxf"

ogr2ogr.main(["", "-f", "DXF", output_dxf,
              reproj_shp, "-nlt",
              "LINESTRING"])
