# ref: https://bit.ly/2xjzJav
#      https://bit.ly/2XqlQCi
#      https://bit.ly/34waBJV
#      

import qgis.core
from qgis.core import *
from qgis.gui import QgisInterface, QgsMapCanvas
from PyQt5.QtCore import QFileInfo, QFile
import qgis.utils
from qgis.utils import iface

strProjectName = "my_project.qgs"

# start the qgis application
QgsApplication.setPrefixPath(r"C:\OSGeo4W64\apps\qgis", True)
qgs = QgsApplication([], False)
qgs.initQgis()

# start a project
project = QgsProject.instance()

# selectedcrs = "EPSG:31984"
# target_crs = QgsCoordinateReferenceSystem()
# target_crs.createFromUserInput(selectedcrs)
# project.setCrs(target_crs)

canvas = QgsMapCanvas()
# iface = QgisInterface.QgsMapCanvas()
project_path = strProjectName

vlayer = QgsVectorLayer("output_files\sub_basin_3d_utm.shp", "sub_basin_3d_utm", "ogr")

project.addMapLayer(vlayer)

dxfExport = QgsDxfExport()
layers = [QgsDxfExport.DxfLayer(vlayer)]
settings = canvas.mapSettings()
dxfExport.setMapSettings( settings )
dxfExport.addLayers( layers )
dxfExport.setLayerTitleAsName(True)
# dxfExport.setDestinationCrs(26191)
dxfExport.setForce2d( False )
dxfFile = QFile( "output_files\sub_basin_3d_utm.dxf" )
dxfExport.writeToFile( dxfFile, 'utf-8')
qgs.exitQgis()
qgs.quit()
