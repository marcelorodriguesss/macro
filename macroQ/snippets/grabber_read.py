import dxfgrabber

dwg = dxfgrabber.readfile("sub_basin_3d_JOSE.dxf")

print(f'DXF version: {dwg.dxfversion}')
print(f"DXF version: {dwg.header['$ACADVER']}")
print(dwg.entities)

# header_var_count = len(dxf.header) # dict of dxf header vars
# layer_count = len(dxf.layers) # collection of layer definitions
# block_definition_count = len(dxf.blocks) #  dict like collection of block definitions
# entity_count = len(dxf.entities) # list like collection of entities

# print(header_var_count)
# print(layer_count)
# print(block_definition_count)
# print(entity_count)