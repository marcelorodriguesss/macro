import shapefile as shp
from dxfwrite import DXFEngine as dxf

shp_path = 'output_files\sub_basin_3d_utm.shp'
sf = shp.Reader(shp_path)
z_points = sf.shapes()[0].z
xy_points = sf.shapes()[0].points

xyz_points = []
for xy, z in zip(xy_points, z_points):
    x, y = xy
    aux = [x, y, z]
    xyz_points.append(tuple(aux))

# print(xyz_points)

# create a new drawing
name = "simple.dxf"
drawing = dxf.drawing(name)

# add a LAYER-tableentry called 'dxfwrite'
# drawing.add_layer('dxfwrite')

# add a VIEWPORT-tableentry
# drawing.add_vport(
#         '*ACTIVE',
#         center_point=(10,10),
#         height = 30,
#     )

polyline = dxf.polyline(xyz_points)
# polyline.add_vertices( xyz_points )
polyline.close()
drawing.add(polyline)
drawing.save()
print("drawing '%s' created.\n" % name)