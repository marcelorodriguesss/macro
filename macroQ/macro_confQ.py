# coding: utf-8

MACRO_CONFIG = {

    # params usados no script ponto.py
    # exemplo do rio 3
    'ponto_leste': '555419.61',
    'ponto_norte': '9524044.62',

    # params usados no script topodata.py
    'nivel_ottobacia': '4',
    'folga_km': '0',

    # ainda nao implementado
    # params usados no script ponto_exultorio.py
    # 'p_exultorio_leste': '556965.961',
    # 'p_exultorio_norte': '9525303.4227',

    # ainda nao implementado
    # params usados no script bacia.py
    # 'inter_curva_niveis': '10',
    # 'anos_min_postos': '23',

    # ainda nao implementado
    # não mude o parametro shp_exultorio;
    # o script bacia.py utiliza e não aceita outro nome para o shape.
    # 'shp_exultorio': 'C:\UFC\UFC11\Shape\Exutorio.shp'

}

# ref: http://bit.ly/2W2FqnR
