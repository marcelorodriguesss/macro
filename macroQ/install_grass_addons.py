import os
from grass_session import Session
import grass.script as gscript

'''
Este script precisa ser executado apenas uma única
vez para instalação dos addons.
'''

# define where to process the data in the temporary grass-session
directory = os.path.dirname(os.path.abspath(__file__))

print("=============================")
print(" Open grass session...")
print("=============================\n")
# create a PERMANENT mapset object: create a Session instance
PERMANENT = Session()

# hint: EPSG code lookup: https://epsg.io
mygisdb = r'{0}\grass'.format(directory)
mylocation = 'newLocation'
PERMANENT.open(gisdb=mygisdb,
               location=mylocation,
               create_opts='EPSG:4326')

# exit from PERMANENT right away in order to perform analysis in our own mapset
PERMANENT.close()

# create a new mapset in the same location
user = Session()
mymapset = 'PERMANENT'
user.open(gisdb=mygisdb,
          location=mylocation,
          mapset=mymapset,
          create_opts='')

print("=============================")
print(" Installing addons...")
print("=============================\n")
# installation of needed addons from grass:
gscript.run_command('g.extension', extension='r.stream.snap', operation='add')
gscript.run_command('g.extension', extension='v.centerpoint', operation='add')
gscript.run_command('g.extension', extension='r.hydrodem', operation='add')
gscript.run_command('g.extension', extension='r.wateroutlet.lessmem', operation='add')

print("=============================")
print(" Done!")
print("=============================\n")
