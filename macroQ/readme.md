PASSO 1 - INSTALAR<br/>
Instalador o OSGeo4W (64 bit)<br/>
Link: https://www.qgis.org/pt_BR/site/forusers/download.html<br/>
Precisa de conexão de Internet.<br/>

PASSO 2 - DOWNLOAD DA PASTA MACROQ:<br/>
Link: https://gitlab.com/marcelorodriguesss/macro<br/>

PASSO 3 - EXECUTAR ARQUIVO:<br/>
Dois cliques no arquivo: ```python-qgis.bat```<br/>
Vai abrir terminal (cmd) com ambiente configurado.<br/>

PASSO 4 - INSTALAR DEPÊNDENCIAS E ADDONS DO GRASS:<br/>
Depois de executar o ```python-qgis.bat```, digitar no terminal:<br/>
```pip install utm```<br/>
```pip install pyshp```<br/>
```pip install dxfwrite```<br/>
```pip install grass_session```<br/>
```python install_grass_addons.py```<br/>
Só precisa executar estes comandos uma única vez.<br/>

PASSO 5 - EDITAR LISTA DE RASTER NO ARQUIVO<br/>
No script ```generate_drainage.py```, editar (qualquer editor de texto)<br/>
a variável ```raster_list``` que contém a lista de arquivos tiffs que<br/>
serão processados.<br/>

PASSO 6 - GERAR DREGAGEM:<br/>
No terminal aberto pelo python-qgis.bat, executar:<br/>
```python generate_drainage.py```<br/>
Em dado momento, script vai solicitar as coordenadas<br/>
lat e lon que representam o exultório da bacia.<br/>
Os arquivos salvos pelo ```generate_drainage.py``` ficam dentro da<br/>
pasta ```output_files```.<br/>
