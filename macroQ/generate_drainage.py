# autor: Marcelo Rodrigues
# e-mail: marcelo.rodrigues@funceme.br
# data: 03 Abr 2020
# Este script for desensenvolvido em parceria com os
# desenvolvedores da FUNCEME e os alunos da disciplina
# de hidraulica avancada do DEHA.

import os
import utm
import time
import shapefile as shp
from dxfwrite import DXFEngine as dxf
from grass_session import Session
import grass.script as gscript
from grass.pygrass.modules.shortcuts import raster as r
from grass.pygrass.modules import Module

import ogr2ogr

import funcs_macroQ

# MARCAR TEMPO DE EXECUÇÃO
start_time = time.time()

nsteps = 12

# EDITAR AQUI!
# LISTA COM TIFF PARA PROCESSAMENTO
# SE HOUVER MAIS DE UM TIF NA LISTA, ELES
# SERÃO UNIDOS (MERGE)
# raster_list = ['04S39_ZN.tif', '04S405ZN.tif']
raster_list = ['04S405ZN.tif']

# -----------------------------------------------------------------
# Start GRASS session and creat directories for the current project
# -----------------------------------------------------------------

# define where to process the data in the temporary grass-session
directory = os.path.dirname(os.path.abspath(__file__))
print("\n=============================")
print(" Pasta de Trabalho:")
print(f" {directory}")
print("=============================\n")

print("=============================")
print(f" Grass Session 1/{nsteps}")
print("=============================\n")
# create a PERMANENT mapset object: create a Session instance
PERMANENT = Session()

# hint: EPSG code lookup: https://epsg.io
mygisdb = r'{0}\grass'.format(directory)
mylocation = 'newLocation'
PERMANENT.open(gisdb=mygisdb,
               location=mylocation,
               create_opts='EPSG:4326')

# exit from PERMANENT right away in order to perform analysis in our own mapset
PERMANENT.close()

# create a new mapset in the same location
user = Session()
mymapset = 'PERMANENT'
user.open(gisdb=mygisdb,
          location=mylocation,
          mapset=mymapset,
          create_opts='')

# ------------------------------------------------------------
# Importing the digital elevation model (DEM) and gerenate the
# drainage network for the whole DEM area
# ------------------------------------------------------------

print("=============================")
print(f" set_epsg 2/{nsteps}")
print("=============================\n")

# os arquivos tiffs do Topodata não possuem
# referencia espacial (SRC).
# a função set_epsg defini a referencia espacial
# e sobreescreve os arquivos de entrada.
for r_raster in raster_list:
    funcs_macroQ.set_epsg(r_raster)

print("\n=============================")
print(f" merge raster 3/{nsteps}")
print("=============================\n")

# define the export module
r.external_out = Module('r.external.out')

# define output directory for files resulting from GRASS calculation:
r.external_out(directory='..\\..\\..\\output_files', format="GTiff")

# FUNCAO PARA FAZER MERGE DOS ARQUIVOS TIFF
# A SAIDA É O ARQUIVO mosaico.tiff DENTRO DA
# PASTA output_files
def merge_raster(raster_list):
    raster_names = 'r1'
    i = 1
    for raster_file in raster_list:
        # Create a mosaic from several inputted rasters
        r.external('e',
                   input=raster_file,
                   output=f'r{i}',
                   overwrite=True)
        if i > 1:
            raster_names += f',r{i}'
        i += 1
    # set the computational region
    gscript.run_command('g.region',
                        raster=raster_names)
    gscript.run_command('r.patch',
                        input=f'{raster_names}',
                        output='mosaico.tiff',
                        overwrite=True)

# SE TIVER MAIS DE UM TIFF PARA FAZER MERGE
if len(raster_list) > 1:
    merge_raster(raster_list)
    mosaic = "output_files\\mosaico.tiff"
else:
    mosaic = raster_list[0]


# set the computational region
print("\n=============================")
print(f" g.region 4/{nsteps}")
print("=============================\n")
# get the raster elevation map into GRASS
r.external(input=mosaic,
           output='raster',
           overwrite=True)
gscript.run_command('g.region', raster='raster')


# r.fillnulls is used to fill null cells in the DEM only if they exist
print("\n=============================")
print(f" r.fillnulss 5/{nsteps}")
print("=============================\n")
gscript.run_command('r.fillnulls',
                    input='raster',
                    output="no_nulls_dem.tiff",
                    method='bilinear',
                    overwrite=True)


print("\n=============================")
print(f" r.hydrodem 6/{nsteps}")
print("=============================\n")
gscript.run_command('r.hydrodem',
                    input="no_nulls_dem.tiff",
                    output='hydro_filled_dem.tiff',
                    overwrite=True,
                    size=4,
                    mod=4)


# run r.watershed only generates the flow direction
# product that will be used in r.water.outlet.
print("\n=============================")
print(f" r.watershed 7/{nsteps}")
print("=============================\n")
gscript.run_command('r.watershed',
                    elevation='hydro_filled_dem.tiff',
                    accumulation='flow_acc.tiff',
                    drainage='flow_direction.tiff',
                    threshold=100,
                    overwrite=True)


# extract the whole drainage network of the DEM
print("\n=============================")
print(f" r.stream.extract 8/{nsteps}")
print("=============================\n")
gscript.run_command('r.stream.extract',
                    elevation='hydro_filled_dem.tiff',
                    stream_raster='drainage_network_raster.tiff',
                    stream_vector='drainage_network_vector',
                    threshold=100,
                    overwrite=True)


print("\n\n=============================")
print(f" v.out.ogr 9/{nsteps}")
print("=============================\n")
gscript.run_command('v.out.ogr',
                    input='drainage_network_vector',
                    layer='1',
                    output='output_files\\segments.geojson',
                    format='GeoJSON',
                    overwrite=True)

print("\n========================================")
print(f" EXPORTANDO PARA TIFF 10/{nsteps}")
print("========================================\n")

#TEST
#-4.72347, -39.55143
#-4.21662, -40.06789

points = []

if not os.path.exists("lat_lon_points.txt"):

    print("\n========================================")
    print(" ARQUIVO lat_lon_points.txt NÃO ENCONTRADO!")
    print("========================================\n")

    print('+++ Entre com as coordenadas do exultório da bacia +++')

    while True:
        try:
            aux_lat = float(input('Digite a latitude  [Ex:  -4.72347]: '))
        except ValueError:
            continue
        if not isinstance(aux_lat, float):
            continue
        else:
            break

    while True:
        try:
            aux_lon = float(input('Digite a longitude [Ex: -39.55143]: '))
        except ValueError:
            continue
        if not isinstance(aux_lon, float):
            continue
        else:
            break

    points.append([aux_lat, aux_lon])

else:

    print("\n========================================")
    print(" LENDO PONTOS DO ARQUIVO: lat_lon_points.txt")
    print("========================================\n")

    with open("lat_lon_points.txt") as fp:
        for line in fp:
            aux_lat = line.strip().split(',')[0]
            aux_lon = line.strip().split(',')[1]
            points.append([float(aux_lat), float(aux_lon)])

# PROCESSAMENTO DAS BACIAS COM BASE NO LAT E LON.

# FAZ PARA TODAS AS COORDENADAS LISTADAS NO ARQUIVO
# lat_lon_points.txt APROVEITANDO A
# DRENAGEM GERADA.

count = 1

for lat, lon in points:

    os.chdir(directory)

    gscript.run_command('g.proj', 'c',
                        proj4='+proj=longlat +datum=WGS84 +no_defs')

    gscript.run_command('r.in.gdal', 'o',
                        input='output_files\\flow_direction.tiff',
                        band=1,
                        output='rast_temp1',
                        overwrite=True)

    gscript.run_command('g.region', raster='rast_temp1')

    gscript.run_command('r.water.outlet',
                        input='rast_temp1',
                        output='rast_temp2',
                        coordinates=[lon, lat],
                        overwrite=True)

    gscript.run_command('g.region', raster='rast_temp2')

    gscript.run_command('r.out.gdal', 't', 'm',
                        input='rast_temp2',
                        output=f'output_files\\{count}_sub_basin.tiff',
                        format='GTiff',
                        createopt='TFW=YES,COMPRESS=LZW')

    print("\n========================================")
    print(f" Arquivo {count}_sub_basin.tiff salvo dentro")
    print(f" da pasta 'output_files'")
    print("========================================")

    print("\n========================================")
    print(f" EXPORTANDO PARA SHAPEFILE 11/{nsteps}")
    print("========================================\n")

    gscript.run_command('g.proj', 'c',
                        proj4='+proj=longlat +datum=WGS84 +no_defs')

    gscript.run_command('r.external', 'o',
                        overwrite=True,
                        input=f'output_files\\{count}_sub_basin.tiff',
                        band=1,
                        output='rast_temp3')

    gscript.run_command('g.region', raster='rast_temp2')

    gscript.run_command('r.to.vect',
                        overwrite=True,
                        input='rast_temp3',
                        type='area',
                        column='value',
                        output='rast_temp4')

    gscript.run_command('v.out.ogr',
                        overwrite=True,
                        type="auto",
                        input="rast_temp4",
                        output=f"output_files\\{count}_sub_basin.shp",
                        format="ESRI_Shapefile")

    # mosaic = "output_files\\mosaico.tiff"  #TEST
    # mosaic = "04S405ZN.tif"                #TEST
    ifile_shp = f"output_files\\{count}_sub_basin.shp"
    dem_file = mosaic  # line 113
    ofile_shp = f"output_files\\{count}_sub_basin_3d.shp"
    funcs_macroQ.setzfromraster(ifile_shp, dem_file, ofile_shp, count)

    print("\n========================================")
    print(f" Arquivo {count}_sub_basin_3d.shp salvo dentro")
    print(" da pasta 'output_files'")
    print("========================================")

    print("\n========================================")
    print(f" EXPORTANDO SHP PARA DXF 12/{nsteps}")
    print("========================================\n")

    print(' == convertendo sistema de coordenadas para utm == ')

    os.chdir(f"{directory}\\output_files")

    input_shp = f"{count}_sub_basin_3d.shp"  # EPSG:4326 WSG84
    reproj_shp = f"{count}_sub_basin_3d_utm.shp"

    # https://github.com/Turbo87/utm
    # https://pypi.org/project/utm/

    # retorna sistema de coordendas sirgas 2000 utm
    # baseado nos pontos lat e lon do exultorio
    # lat, lon = -4.72347, -39.55143  #TEST
    easting, nothing, z_number, z_letter = utm.from_latlon(lat, lon)
    new_epsg = funcs_macroQ.utm2sirgas2000(z_number, z_letter)

    # muda sistema de coordenadas

    # ogr2ogr.main(["", "-s_srs", "EPSG:4326",
    #             # "-t_srs", f"EPSG:{new_epsg}"
    #             # "-t_srs", "EPSG:4674"
    #             "-t_srs", "EPSG:31984"
    #             # "-t_srs", "+proj=utm +zone=24 +south +units=m +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs"
    #             "-f", reproj_shp,
    #             input_shp])

    ogr2ogr.main(["", "-f", "ESRI Shapefile",
                  "-t_srs", f"EPSG:{new_epsg}",
                  reproj_shp, input_shp])

    output_dxf = f"{count}_sub_basin_3d.dxf"

    # escreve dxf
    sf = shp.Reader(reproj_shp)
    z_points = sf.shapes()[0].z
    xy_points = sf.shapes()[0].points

    xyz_points = []
    for xy, z in zip(xy_points, z_points):
        x, y = xy
        aux = [x, y, z]
        xyz_points.append(tuple(aux))

    drawing = dxf.drawing(output_dxf)
    polyline = dxf.polyline(xyz_points)
    polyline.close()
    drawing.add(polyline)
    drawing.save()

    print("\n========================================")
    print(f" Arquivo {count}_sub_basin_3d.dxf (UTM) salvo dentro")
    print(" da pasta 'output_files'")
    print(" No AutoCAD, use 'zoom extents' para visualizar!")
    print("========================================\n\n")

    count += 1

print("\n========================================")
print(f" Tempo de execução: {(round((time.time() - start_time), 2))}s")
print("========================================\n")
