# coding: utf-8

import os
import arcpy
import shutil
import time

start_time = time.time()

arcpy.AddMessage(' ='*30)

# Diretorio Raiz
UFC11  = r'C:\UFC\UFC11'

# Remover Pastas

Raster = r'{0}\Raster'.format(UFC11)
Saida  = r'{0}\Saida'.format(UFC11)
Shape  = r'{0}\Shape'.format(UFC11)
Ponto  = r'{0}\Dados\Ponto'.format(UFC11)

list_dirs = [Raster, Shape, Ponto]  # Saida

for dirs in list_dirs:
    shutil.rmtree(dirs, ignore_errors=True)
    arcpy.AddMessage(' + Removendo Pasta:    {0} - OK!'.format(dirs))

# Remover Arquivos

APAL     = r'{0}\ALOSPALSAR.txt'.format(UFC11)
PAnalise = r'{0}\Dados\PAnalise.shp'.format(UFC11)
PAnalise_bkp = r'{0}\Dados\PAnalise_bkp.zip'.format(UFC11)

list_files = [APAL, PAnalise, PAnalise_bkp]

for files in list_files:
    try:
        os.remove(files)
        arcpy.AddMessage(' + Removendo Arquivo:  {0} - OK!'.format(files))
    except OSError:
        arcpy.AddMessage(' + Removendo Arquivo:  {0} - OK!'.format(files))
        arcpy.AddMessage(' + Arquivo não existe: {0} - OK!'.format(files))

# Criar Pastas

Topodata = r'{0}\Topodata'.format(UFC11)
RasterAP = r'{0}\RasterAP'.format(UFC11)

list_dirs = [Raster, Saida, Shape, Ponto, Topodata, RasterAP]

for dirs in list_dirs:
    if not os.path.exists(dirs):
        os.makedirs(dirs)
        arcpy.AddMessage(' + Criando Pasta:      {0} - OK!'.format(dirs))

end_time = time.time()

run_time = end_time - start_time

arcpy.AddMessage(" + Tempo de execucao:  {0}s".format(run_time))

arcpy.AddMessage(' ='*30)
