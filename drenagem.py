# coding: utf-8

import arcpy
import multiprocessing
import time

start_time = time.time()

arcpy.AddMessage(' ='*30)

# Verificar as licencias
arcpy.CheckOutExtension("spatial")
arcpy.CheckOutExtension("3D")

# Pegar o Arquivo Raster do usuario
Raster = arcpy.GetParameterAsText(0)
if Raster == '#' or not Raster:
    Raster = "C:\UFC\UFC11\Raster\REXT.tif" # fornecido pelo usuario
# Pegar o Poligono de Corte do usuario

Polig = arcpy.GetParameterAsText(1)
if Polig == '#' or not Polig:
    arcpy.AddMessage("")
else:
    # Iniciar Variavel Temporal
    Rtemp = "C:\UFC\UFC11\Raster\RTTD.tif"
    # Apagar Arquivo Anterior
    arcpy.Delete_management(Rtemp, "RasterDataset")
    # Cortar o Raster com o Poligono
    arcpy.gp.ExtractByMask_sa(Raster, Polig, Rtemp)
    # Apagar Raster Anterior
    arcpy.Delete_management(Raster, "RasterDataset")
    # Renomear Arquivo Raster Cortado
    arcpy.Rename_management(Rtemp, Raster, "RasterDataset")

# Variaveis Locais
TRel = "C:\UFC\UFC11\Raster\emrrel.tif"
FD = "C:\UFC\UFC11\Raster\FD.tif"
FA = "C:\UFC\UFC11\Raster\FA.tif"
RC = "C:\UFC\UFC11\Raster\mcal.tif"
SL = "C:\UFC\UFC11\Raster\slink.tif"
SO = "C:\UFC\UFC11\Raster\sorder.tif"
Rios = "C:\UFC\UFC11\Shape\Rios2Dt.shp"
Rios2D = "C:\UFC\UFC11\Shape\Rios2D.shp"
VRios = "C:\UFC\UFC11\Shape\VRios.shp"
Shape = "C:\UFC\UFC11\Shape"
rtemp = "C:\UFC\UFC11\Shape\prtemp.shp"

# Apagar arquivos anteriores
arcpy.AddMessage(" + Apagando Arquivos Anteriores")
arcpy.Delete_management(TRel, "RasterDataset")
arcpy.Delete_management(FD, "RasterDataset")
arcpy.Delete_management(FA, "RasterDataset")
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(Rios2D, "ShapeFile")
arcpy.Delete_management(VRios, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")

# Encher os Buracos do DEM
arcpy.AddMessage(u" + Preenchendo depressões do DEM")
arcpy.gp.Fill_sa(Raster, TRel, "")

# Obter o Flow Direction do DEM
arcpy.AddMessage(" + Calculo da Direcao do Fluxo do DEM")
arcpy.gp.FlowDirection_sa(TRel, FD, "NORMAL", "")

# Obter o Flow Accumulation do DEM
arcpy.AddMessage(" + Calculo do Fluxo Acumulado do DEM")
arcpy.gp.FlowAccumulation_sa(FD, FA, "", "FLOAT")

# Clasificar os Rios pela quantidade de area drenada
# Importar a Livraria Spatial Analyst
from arcpy.sa import *

# Indicar o valor minimo do FA
acum = 4

# Condicao do Raster
outRaster = Raster(FA) > acum

# Salvar o Raster
outRaster.save(RC)

# Unir os Pontos continuos dos Rios
arcpy.gp.StreamLink_sa(RC, FD, SL)

# Definir a Ordem dos Rios
arcpy.AddMessage(" + Classificando Rios")
arcpy.gp.StreamOrder_sa(SL, FD, SO, "STRAHLER")

# Exportar os rios a Shapefile
arcpy.AddMessage(" + Exportando Rios para Shapefile")
arcpy.gp.StreamToFeature_sa(SO, FD, Rios, "SIMPLIFY")

# Criar Shapefile com Propiedades Ativas de Cotas
arcpy.CreateFeatureclass_management(Shape, "prtemp.shp", "POLYLINE", "", "ENABLED", "ENABLED", "GEOGCS['GCS_WGS_1984',DATUM['D_WGS_1984',SPHEROID['WGS_1984',6378137.0,298.257223563]],PRIMEM['Greenwich',0.0],UNIT['Degree',0.0174532925199433]];-400 -400 1000000000;-100000 10000;-100000 10000;8.98315284119522E-09;0.001;0.001;IsHighPrecision", "", "0", "0", "0")

# Pegar as Caracteristicas do Shape
arcpy.Merge_management("C:\\UFC\\UFC11\\Shape\\prtemp.shp;C:\\UFC\\UFC11\\Shape\\Rios2Dt.shp", Rios2D, "")

# Exportar os Vertices a Pontos
arcpy.AddMessage(" + Exportando Vertices dos Rios para Shapefile")
arcpy.FeatureVerticesToPoints_management(Rios2D, VRios, "ALL")

# Apagar Arquivos Temporais
arcpy.AddMessage(" + Apagando Arquivos Temporarios")
arcpy.Delete_management(TRel, "RasterDataset")
arcpy.Delete_management(RC, "RasterDataset")
arcpy.Delete_management(SL, "RasterDataset")
arcpy.Delete_management(SO, "RasterDataset")
arcpy.Delete_management(Rios, "ShapeFile")
arcpy.Delete_management(rtemp, "ShapeFile")

# Verificar as licencias
arcpy.CheckInExtension("Spatial")

# Visualizar os arquivos resultados
# Importar Livraria mapping
import arcpy.mapping

# Definir os Arquivos Shape a inserir
arquivo1 = r"C:\UFC\UFC11\Dados\PAnalise.shp"
arquivo2 = r"C:\UFC\UFC11\Shape\Rios2D.shp"
arquivo3 = r"C:\UFC\UFC11\Shape\VRios.shp"

arcpy.AddMessage("\n + Abrir os seguintes arquivos no ArcGIS")
arcpy.AddMessage("   para escolher o ponto que representa o exultorio:")
arcpy.AddMessage(" + {0}".format(arquivo1))
arcpy.AddMessage(" + {0}".format(arquivo2))
arcpy.AddMessage(" + {0}\n".format(arquivo3))

end_time = time.time()

run_time = end_time - start_time

arcpy.AddMessage(" + Tempo de execucao: {0}s".format(run_time))

arcpy.AddMessage(' ='*30)

raise SystemExit

# Definir o Arquivo de ArcGIS atual.
tatual = arcpy.mapping.MapDocument("Current")

# Definir o Layer Atual
camadaatual = arcpy.mapping.ListDataFrames(tatual,"*")[0]

# Criar uma Nova Camada
camada1 = arcpy.mapping.Layer(arquivo1)
camada2 = arcpy.mapping.Layer(arquivo2)
camada3 = arcpy.mapping.Layer(arquivo3)

# Adicionar a Nova Camada no arquivo atual
arcpy.AddMessage("Adicionando Arquivos na Tela Atual")
arcpy.mapping.AddLayer(camadaatual, camada3,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada2,"Bottom")
arcpy.mapping.AddLayer(camadaatual, camada1,"Bottom")

# Atualizar los Arquivos
arcpy.RefreshActiveView()
arcpy.RefreshTOC()

# Zoom Shapefile PAnalise.shp
# Escolher o Documento Atual
mxd = arcpy.mapping.MapDocument('current')

# Escolher o Layer Atual
df = arcpy.mapping.ListDataFrames(mxd)[0]

# Escolher o Shape da Bacia
lyr = arcpy.mapping.ListLayers(mxd, 'PAnalise*', df)[0]

# Definir a Variavel pra fazer Zoom
ext = lyr.getExtent()

# Fazer Zoom to Layer
df.extent = ext
df.scale = 5000
arcpy.RefreshActiveView()
